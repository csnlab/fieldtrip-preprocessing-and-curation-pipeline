My aim with this pipeline was to make something that others can use to speed up preprocessing and curation of LFP data.
The workflow of this pipeline is similar to what fieldtrip itself advocates in [their walkthrough](https://www.fieldtriptoolbox.org/walkthrough/#foreword), but with some adaptations to make the process more suitable for LFP (instead of EEG/MEG).


Run this pipeline in the following sequence:

1- If you have raw data, use the files in the preprocessing folder. The master script here is jd_preprocessingCC. You can of course use your own script for this, but if you don't it's helpful knowing some particularities of how I handle my data at this stage:
- the pipeline is currently designed for Neuralynx data and includes lines for getting trial events from two types of events (trials and baselines in my case).
- each of my raw data folders also contains a .mat file containing all information pertaining to that session (e.g. trial parameters, stimulus settings, etc.)
- the function jd_define_trl creates the cfg.trl matrix but also couples this to information about each trial. ft_preprocessing automatically allocates the extra columns in the cfg.trl to a field called trialinfo.

2- If you already ran ft_preprocessing then you can open the master script jd_artefact_rejection in the curation folder. This script contains extra information about how to use the pipeline.  The main steps are:
    - remove only the most horendous trials and channels;
    - find segments of clipping data and remove/repair these traces;
    - perform ICA and manually isolate and remove artefactuous components from your data.

Finally, yhere is also a master script called jd_exploratory_analysis that may or may not be helpful. Some of the functions allow you to set baseline windows, find the right parameters for your filterbank and perform baseline correction on the time-frequency data.
