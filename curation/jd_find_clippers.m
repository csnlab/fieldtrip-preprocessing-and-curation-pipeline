function Data = jd_find_clippers(Data, timethreshold, amplthreshold,margin)
% this function is part of the FPPP pipeline. It locates clipping data segments
% and shows you random plots so you can see whether you like the current
% parameters. It's a bit underdeveloped in terms of being user friendly but
% it does a very good job.
%
% ******************CHANGELOG******************
% created:      -24/12/2020-
%               -21-01-2021-
% function can now handle datasets with varying trial lengths

% store input arguments
Data.clipParams = struct('timeThreshold',timethreshold,'amplitudeThreshold',amplthreshold,'margin',margin);

% set parameters
halfTime = round(0.5*timethreshold * Data.fsample);
margin = round(margin * Data.fsample);
info.amp = amplthreshold;
info.Data = Data;
info.cont = true;
info.absdiff = false;

% find clipping segments
[lengths, ~, Li] = unique(cell2mat(cellfun(@(x) numel(x),Data.time,'UniformOutput',false)));
info.clip = cell(1,numel(Data.time));
info.absDiff = cell(size(info.clip));
for ti = 1:numel(lengths)
    index = Li == ti;
    clp = Data.trial(index);
    clp = abs(gradient(cat(3,clp{:})));
    tmp = movmean(clp <= amplthreshold,[halfTime,halfTime],2) == 1;
    tmp = add_margins(tmp,margin,halfTime);
    info.clip(index) = squeeze(mat2cell(tmp,size(tmp,1),size(tmp,2),repelem(1,size(tmp,3))));
    info.absDiff(index) = squeeze(mat2cell(clp,size(clp,1),size(clp,2),repelem(1,size(clp,3))));
end
clear tmp clp

% present examples to user for verification
clipTraces = cell2mat(cellfun(@(x) any(x,2),info.clip,'UniformOutput',false));
if any(clipTraces, 'all')
    [info.r, info.c] = find(clipTraces);
    info.nClips = numel(info.r);
    info.randOrder = randperm(info.nClips);
    info.itrl = 1;
else
    msgbox('no clipping trials detected');
    info.cont = 0;
end

f = figure;
uicontrol(f,'units','normalized','position',[0.93 0.46 0.03 0.03],'String','<','Callback',@prev);
uicontrol(f,'units','normalized','position',[0.96 0.46 0.03 0.03],'String','>','Callback',@next);
info.ui.msgGen = uicontrol(f,'units','normalized','position',[0.93 0.35 0.06 0.10],'String','standby','FontSize',14,'FontWeight','bold','Style','text');
info.exit  = uicontrol(f,'units','normalized','position',[0.93 0.15 0.06 0.03],'String',"I've seen enough",'Callback',@quit);
info.inspect = uicontrol(f,'units','normalized','position',[0.93 0.50 0.06 0.03],'String',"absdiff",'Callback',@absdiff);
guidata(f,info);

while info.cont
    redraw(f);
    uiwait(f);
    info = guidata(f);
%     if info.absdiff
%         figure;
%         plot(info.clipDat(r(idx),:,c(idx)));
%         hold on
%         plot(info.clip(r(idx),:,c(idx)).*50);
%         hold off
%         drawnow;
%         info.absdiff = false;
%     end
end
Data.clip = info.clip;
delete(f);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function redraw(f,~)
info = guidata(f);
idx = info.randOrder(info.itrl);
info.idx = idx;
plot(info.Data.time{info.c(idx)},info.Data.trial{info.c(idx)}(info.r(idx),:));
lim = get(gca,'YLim')*2;
hold on
plt = area(info.Data.time{info.c(idx)},info.clip{info.c(idx)}(info.r(idx),:).*(abs(lim(1))+abs(lim(2)))+(lim(1)),lim(1),'FaceColor',[1 0 0]);
plt.EdgeColor = 'none';
plt.FaceAlpha = 0.1;
hold off
title(sprintf('Trial %d: \t Channel: %s(%d)',info.c(idx), info.Data.label{info.r(idx)},info.r(idx)));
ylim(lim./2);
info.ui.msgGen.String = sprintf('trace: \n%i/%i',info.itrl,numel(info.randOrder));
guidata(f,info);
drawnow;
end

function next(f,~)
info = guidata(f);
if info.itrl < numel(info.randOrder)
    info.itrl = info.itrl+1;
else
    msgbox('you have seen all detected clippers');
end
guidata(f,info);
uiresume;
end

function prev(f,~)
info = guidata(f);
if info.itrl > 1
    info.itrl = info.itrl-1;
end
guidata(f,info)
uiresume;
end

function absdiff(f,~)
info = guidata(f);
idx = info.randOrder(info.itrl);
xlimit = [info.Data.time{info.c(idx)}(1),info.Data.time{info.c(idx)}(end)];
figure;
plot(info.Data.time{info.c(idx)},info.absDiff{info.c(idx)}(info.r(idx),:));
hold on
plot(info.Data.time{info.c(idx)},info.clip{info.c(idx)}(info.r(idx),:).*50);
plot(xlimit,[info.amp,info.amp],'r-');
hold off
xlim(xlimit);
ylim([0,60]);
title('absdiff');
xlabel('time');
ylabel('abs(diff(signal)) (\muV)');
legend({'trace','clipmark','amp threshold'});
drawnow;
info.absdiff = false;
guidata(f,info);
uiresume;
end

function quit(f,~)
info = guidata(f);
info.cont = false;
guidata(f,info);
uiresume;
end

function out = add_margins(clip,mar,ht)
nChan = size(clip,1);
nTrl = size(clip,3);
diffclip = [clip(:,1,:),diff(clip,[],2)]; % find edges of clipping segments
htm = ht+mar;
% ea = sum(diffclip(:,1:htm,:),2) == -1 | sum(logical(diffclip(:,1:htm,:)),2) >= 2; % trim early edge artefacts
% diffclip(:,1:htm,:) = diffclip(:,1:htm,:).* ~repmat(ea,1,htm,1);
% la = sum(diffclip(:,end-(htm-1),:),2) == -1 | sum(logical(diffclip(:,end-(htm-1):end,:)),2) >= 2; % trim late edge artefacts
% diffclip(:,end-htm+1:end,:) = diffclip(:,end-(htm-1):end,:).* ~repmat(la,1,htm,1);
% Beg = any(diffclip(:,1:htm,:),2); % fill beginning in case margin < tt 
% End = any(diffclip(:,end-htm+1:end,:),2); % fill end in case margin < tt 
Beg = sum(diffclip(:,1:htm,:) == 1, 2); % fill beginning in case margin < tt 
% End = any(diffclip(:,end-htm+1:end,:) == 1, 2); % fill end in case margin < tt 

% Mid = diffclip(:,htm+2:end-htm-1,:);
pos = [Beg,diffclip(:,htm+2:end,:)== 1,zeros(nChan,htm,nTrl)] ; % get margin start points
% neg = [zeros(nChan,htm,nTrl),diffclip(:,1:end-htm-1,:),End] == -1; % get margin end points
neg = [zeros(nChan,htm,nTrl),diffclip(:,1:end-htm,:)== -1] ; % get margin end points

% pos = [Beg,diffclip(:,mar+2:end-mar,:),zeros(nChan,mar*2,nTrl)] == 1; % get margin start points
% neg = [zeros(nChan,mar*2,nTrl),diffclip(:,mar+1:end-mar-1,:),End] == -1; % get margin end points
out = logical(cumsum(pos-neg,2)); 
end

%% debug plots
% % % trl = 297;
% % % chan = 2;
% % % 
% % % figure;
% % % plot(Data.trial{trl}(chan,:));
% % % title('Data');
% % % 
% % % figure;
% % % plot(clp(chan,:,trl));
% % % title('absdiff');
% % % 
% % % figure;
% % % plot(clip(chan,:,trl));
% % % title('clip');
% % % 
% % % figure;
% % % plot(diffclip(chan,:,trl));
% % % title('diffclip');
% % % 
% % % 
% % % figure;
% % % plot(out(chan,:,trl));
% % % title('out');