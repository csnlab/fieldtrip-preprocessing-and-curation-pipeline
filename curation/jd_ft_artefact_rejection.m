% this is the master script for artefact rejection. The purpose of
% this script is to remove all artefacts while retaining as much data as
% possible. Your data should have gone through ft_preprocessing in order to
% work here.
%
% HOW TO:
% press ctrl+enter to run each segment. You can skip steps and segments,
% but you cannot return to previous steps.
% 
% ******************CHANGELOG******************
% created:      -23/12/2020-
%               -04/01/2021-
% - added jd_reject_clippers and jd_interpolate to pipeline
%               -21-01-2021-
% - pipeline can now handle datasets with varying trial lengths
% - cleaned up commenting throughout pipeline
%               -12-03-2021
% - added delta function to inspectral

%% load fieldtrip
addpath 'C:\Program Files\MATLAB\R2018a\toolbox\fieldtrip-20191028';
ft_defaults
addpath 'C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\concentric circles\analysis'
%% load preprocessed data
root = "C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\concentric circles";
addpath(genpath('C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\fieldtrip pipeline'));
dataDir = fullfile(root, 'processed_data');

oldDir = cd(dataDir); % choose data folder as current directory
session = uigetfile; % user-friendly way to load filenames
load(session); % file was previously named 'processedData'
cd(oldDir);

%% step 1: first inspection of the data and removal of artefactuous channels and trials
% removing bad channels improves subsequent component-wise artefact
% rejection and allows you to salvage more data.
% rejecting clipping traces is not necessary since that will be done in
% step 2. Try to only remove channels that are extremely flat or noisy, and
% only trials that are totally unsalvageble.
cfg = [];
cfg.alim     = 1e3; % scales the y axis but has no effect on the data
cfg.keeptrial = 'nan'; % don't set to 'no' because it will misalign the correspondence between trials and conditions
cfg.keepchannel = 'no'; % keep at 'no'
cfg.method = 'channel'; % 'summary' | 'trial' | 'channel' (I find the range statistic especially helpful in summary mode)

processedData = ft_rejectvisual(cfg,processedData);

%% step 2: identify and remove/repair clipping
% in this step you can identify segments of clipping data and then remove
% or repair those data.

% jd_find_clippers detects segments of clipping data based on the absolute
% difference of the original trace (absdiff).
% to avoid false negatives, start out with more liberal parameters and work
% your way down.

amplthreshold = 4; % absdiff amplitude (microvolts)
timethreshold = 0.1; % minimum time (seconds) below amplitude threshold to be considered clipping 
margin = 0.15; % overshoot of true clipping margins (seconds)
processedData = jd_find_clippers(processedData, timethreshold, amplthreshold, margin);

%% reject trials with excessive clipping
% remove trials/channels where many channels show clipping (proper interpolation
% won't work for these trials).
% The amount of clipping in a trace is less important if you plan to 
% interpolate the data before jd_reject_components.
processedData = jd_reject_clippers(processedData);

%% interpolate remainder of clipping data
% replacing clipping data with interpolated data improves the performance
% of ft_componentanalysis, especially if there is more clipping or clipping
% is concentrated in certain channels.
% NOTE: this function tries to ascertain the probe mapping from the same
% .prb file that Kilosort uses. If the probe has more than one shank, it
% also attempts to ascertain the distance between shanks from the filename.
% If this part of the function does not work for you, my advice is that you
% create your own probe mapping matrix and circumvent the probe mapping
% subfunction.

% retaining highlights allows you to see the results of your interpolation in the jd_reject_components GUI
retainClipHighlights = true; 

prb_path = 'C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\concentric circles\probe mapping info';
prbFile = 'CM1x32_CSCflipped.prb'; % 'A4x8-5mm-100-200-177-CM32_moduleshifted.prb';
% % % load(fullfile(prb_path, 'CSC2AD.mat')); % mapping between AD channels and CSC
processedData = jd_interpolate(processedData,fullfile(prb_path,prbFile),'interpolate', retainClipHighlights);
%% step 3: decompose the data, reject artefactuous components and project data back to sensor level with those components partialled out
% perform ICA. of all the methods I've tried runica seems superior for LFP
% data
nonNaNTrials = cell2mat(cellfun(@(x) ~all(isnan(x),'all'),processedData.trial,'UniformOutput',false));
firstnonNaN = find(nonNaNTrials,1);
tmp = processedData;
tmp.time = [tmp.time(firstnonNaN),tmp.time];
tmp.trial = [tmp.trial(firstnonNaN),tmp.trial];
cfg = [];
cfg.method = 'runica';
comp = ft_componentanalysis(cfg,tmp); % this might take a few minutes (make sure you downsampled your data)
clear tmp
comp.trial = comp.trial(2:end);
comp.time = comp.time(2:end);

% start GUI to reject components of the data. The GUI saves a copy of the original
% data, so you can store your progress and continue later. In view of file
% size, the original data will be deleted from the structure when it is renamed
% to curatedData (see below).
cfg.alim     = 1e3;
cfg.removeUnivalentComps = false; % false | true | [vector of component indeces to be removed]
processedData = jd_reject_components(processedData, comp, cfg);

%% inspect correlations between components and conditions
% if one or more components load particularly heavy on a condition, then
% this could indicate that you removed some relevant signal.

% example:
% make boolean vectors for your conditions of interest
conditions = [processedData.trialinfo(:,1) == 0, ... % incongruent
              processedData.trialinfo(:,1) == 1, ... % congruent
              processedData.trialinfo(:,1) == 2, ... % audio only
              processedData.trialinfo(:,1) == 999, ... % baseline trials
              processedData.trialinfo(:,2), ... % visual tf
              processedData.trialinfo(:,2), ... % audio tf
              ];
% correlate
nonNaNTrials = cell2mat(cellfun(@(x) ~all(isnan(x),'all'),processedData.trial,'UniformOutput',false));
[rho, pval] = corr(conditions(nonNaNTrials,:),processedData.rmcomps(:,nonNaNTrials)','Type','Spearman');

% and plot (components are NaN if they were never removed/kept, i.e. var=0)
plt = heatmap(rho);
title('correlation between conditions and components');
xlabel('component');
ylabel('condition');

%% step 4: remove unwanted data from curated dataset

% rename
curatedData = processedData;

% remove rejected trials from trialinfo field and baseline array
curatedData.rmtrls = ~cell2mat(cellfun(@(x) ~all(isnan(x),'all'),curatedData.trial,'UniformOutput',false)); % keep a record of rejected trials
curatedData.trial = curatedData.trial(~curatedData.rmtrls);
curatedData.time = curatedData.time(~curatedData.rmtrls);
curatedData.trialinfo = curatedData.trialinfo(~curatedData.rmtrls,:);
curatedData.sessionData.baselinetrials = curatedData.sessionData.baselinetrials(~curatedData.rmtrls); % specific for my data

if isfield(curatedData,'rmcomps') % only works if you ran step 3
    curatedData = rmfield(curatedData,'origData'); % remove original data
    curatedData.rmcomps = curatedData.rmcomps(:,~curatedData.rmtrls); % keep a record of rejected components
end

% delete NaN channels (deprecated)
% % % keepchannels = ~isnan(curatedData.trial{1}(:,1)); % this will become disfunctional if you delete single channels per trial
% % % curatedData.trial = cellfun(@(x) x(keepchannels,:), curatedData.trial, 'UniformOutput', false);
% % % curatedData.label = curatedData.label(keepchannels);

%% save inspected session
fileName = [session(1:end-4), '_cur.mat'];
save(fullfile(dataDir,fileName), 'curatedData', '-v7.3');
