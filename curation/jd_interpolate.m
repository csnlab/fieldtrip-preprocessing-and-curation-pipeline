function Data = jd_interpolate(Data,prb_path,method, rch)
% this function replaces clipping data segments (per whole trialxchannel)
% with interpolated data, using a method of your choice. Fieldtrip doesn't
% support this functionality for electrodes oriented non-spherically (e.g.
% EEG, MEG). 
%
% To do:
% - add sanity check to probe file formatting subfunction
%
% ******************CHANGELOG******************
% created:      -04/01/2021-
%               -21-01-2021-
% function can now handle datasets with varying trial lengths
%               -28-01-2021-
% corrected probe mapping (previous version mapped AD channels to CSC
% channels)
%               -08-03-2021-
% new and more flexible probe subfunction that works without CSC2AD mapping
    
% prepare probe mapping matrix (channels,[x,y])
CSC2AD = false; % redundant
probe = format_prb(prb_path, CSC2AD); 
% show plot of channel locations
response = show_layout(probe);
delete(gcf);
if strcmp(response, 'no')
    return
end
chanNums = cell2mat(cellfun(@(x) str2double(regexp(x,'\d*','match')),Data.label, 'UniformOutput', false));
[~, ~, chanOrd] = intersect(chanNums,probe(:,1),'stable');
% while numel(chanOrd) ~= numel(chanNums) % patch for personal use
%     probe(:,1) = probe(:,1) - 1;
%     [~, ~, chanOrd] = intersect(chanNums,probe(:,1),'stable');
% end
probe = probe(chanOrd,2:3);

% generate channel weighting matrix

% determine which channels can be interpolated

% set method and loop through trials
clipTrials = find(cell2mat(cellfun(@(x) any(x,'all'),Data.clip,'UniformOutput',false)));
ti = 1;
Nt = numel(clipTrials);
for trl = clipTrials
    % find clipping channels
    badChans = any(Data.clip{trl},2);
    if all(badChans) % if all channels are clipping
        warning(sprintf('skipping trial %d because all trials are clipping',trl));
        ti = ti+1;
        continue
    else
        fprintf('repairing cliptrial %d/%d\n',ti,Nt)
        ti = ti+1;
    end
    switch method
        case 'average'
            Data.trial{trl}(badChans,:) = mean(Data.trial{trl}(~badChans,:));
        case 'interpolate'
            if numel(unique(probe(:,1))) ~= 1
                for t = 1:size(Data.trial{trl},2)
    % % %                 test(:,t) = Data.trial{trl}(:,t);
                    F = scatteredInterpolant(probe(~badChans,:),Data.trial{trl}(~badChans,t),'natural','nearest'); % interpolation function
                    Data.trial{trl}(badChans,t) = F(probe(badChans,1),probe(badChans,2)); % interpolate from trusted channels
                end
            else % use similar interpolation method (see also doc spline/pchip/makima)
                [depths, shallow2deep] = sort(probe(:,2)); % sort channel y positions to find clippers at extreme ends (these cannot be interpolated)
                if badChans(shallow2deep(1)) == true % correct upper channels
                    upperNaNs = find(~badChans,1,'first');
                    Data.trial{trl}(shallow2deep(1:upperNaNs),:) = repmat(Data.trial{trl}(shallow2deep(upperNaNs),:),upperNaNs,1);
                    badChans(shallow2deep(1:upperNaNs)) = false;
                end
                if badChans(shallow2deep(end)) == true % correct lower channels
                    lowerNaNs = find(~badChans,1,'last');
                    Data.trial{trl}(shallow2deep(lowerNaNs:end),:) = repmat(Data.trial{trl}(lowerNaNs,:),numel(depths)-lowerNaNs+1,1);
                    badChans(shallow2deep(lowerNaNs:end)) = false;
                end
                if any(badChans)
                    for t = 1:size(Data.trial{trl},2)
                        Data.trial{trl}(:,t) = makima(depths(~badChans),Data.trial{trl}(~badChans,t),depths);
                    end
                end                
            end
        case 'weighted'
            % something like scatteredInterpolant, but with a sharper decay
            % function
            fprintf('\npoop');
        case 'weightedLayered'
            % like weighted, but restricted to electrodes at similar depth
        case 'makima'

        otherwise
            error('requested method does not exist');
    end
% % %     chan2Check = 4;
% % %     figure;
% % %     ax1 = subplot(2,2,1);
% % %     imagesc(ax1,test);
% % %     title('original data');
% % %     ax2 = subplot(2,2,2);
% % %     imagesc(ax2,Data.trial{trl});
% % %     c = max([ax1.CLim;ax2.CLim],[],1);
% % %     ax1.CLim = c; ax2.CLim = c;
% % %     title('data after interpolation');
% % %     ax3 = subplot(2,2,3);
% % %     plot(ax3,test(chan2Check,:));
% % %     ax4 = subplot(2,2,4);
% % %     plot(ax4,Data.trial{trl}(chan2Check,:));

end
if ~rch % if retainClippingHighlights is false, remove all remaining highlights
    for cli = clipTrials
        Data.clip{cli}(:,:) = false;
    end
end
end

function probe = format_prb(prb_path, CSC2AD)
% returns 3xN cell array containing all probe mapping info. columns correspond
% to shanks, rows correspond to channels, geometry and graph properties.
% probe files and filenames must be formatted as seen in:
% https://github.com/kwikteam/probes/tree/master/neuronexus
handle = fopen(prb_path);
A = textscan(handle,'%s');
CA = cat(2,A{1}{:});
% A = importdata(prb_path);
% CA = cat(2,A{:});

% find indeces of strings that signal data meaning
fieldNames = {'channels','geometry','graph','label'};
present = true(numel(fieldNames),1);
idxs = cell(numel(fieldNames),2);
for fi = 1:numel(fieldNames)
    [tmp1, tmp2] = regexp(CA,fieldNames{fi});
    if isempty([tmp1, tmp2]) % indicates field does not exist in probe file
        present(fi) = false;
    else
        idxs{fi,1} = tmp1;
        idxs{fi,2} = tmp2;
    end
end
iGeo = find(contains(fieldNames(present),'geometry'));
idxs = idxs(present,:);
assert(isfinite(iGeo),'geometry field must be present in probe file');
Nshanks = numel(idxs{1}); 
probe = [];

% get inter-shank distance from probe filename. This gives you the x
% coordinates of the electrodes and allows you to map the channels spatially as a
% grid.
if Nshanks > 1
    [~, filename, ~] = fileparts(prb_path);
    ISD = str2double(regexp(filename,'\d*','match'));
    ISD = ISD(5); % fifth number in filename
    response = questdlg(sprintf('distance between shanks is %d microns, correct?',ISD),'set ISD','yes','no','yes');
    if strcmp(response,'no')
        msgdlg('input inter-shank distance in command window to continue');
        ISD = input(sprintf('distance between shanks (micrometer): \n'));
    end
else
    ISD = 0;
end

% assign data to cells
for shi = 1:Nshanks
% %     % fill channel cell
% %     probe{1,shank} = str2double(regexp(CA(cha2(shank):geo1(shank)),'\d*','match'));
    % fill geometry cell
    shank = reshape(str2double(regexp(CA(idxs{iGeo,2}(shi):idxs{iGeo+1,1}(shi)),'\d*+\.?\d*','match')),3,[]);
    shank(2,:) = ISD * (shi-1);
    probe = [probe; shank'];
% %     % fill graph cell
% %     try
% %         probe{3,shank} = str2double(regexp(CA(gra2(shank):cha1(shank+1)),'\d*','match'));
% %     catch
% %         probe{3,shank} = str2double(regexp(CA(gra2(shank):end),'\d*','match'));
% %     end
% %     if mod(length(probe{3,shank}),2)
% %         probe{3,shank} = probe{3,shank}(1:end-1);
% %     end
% %     probe{3,shank} = reshape(probe{3,shank},2,[]);
end
% % % % remap AD to CSC values
% % % [~, ~, C2Aorder] = intersect(probe(:,1),CSC2AD(:,2),'stable');
% % % probe(:,1) = CSC2AD(C2Aorder,1);

        
end
function response = show_layout(probe)
x = probe(:,2);
y = -probe(:,3);
num = probe(:,1);

figure;
axis([min(x)-100, max(x)+100, min(y)-100, max(y)+100]);
for k=1:length(x)
   text(x(k),y(k),num2str(num(k)))
end
title('probe site numbers and locations')
ylabel('depth (\mum)')
xlabel('x position (\mum)')
% set(gca,'YDir','reverse')
grid minor
drawnow

response = questdlg('is this mapping correct?','confirm mapping','yes','no','yes');

end
