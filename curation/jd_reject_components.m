function clean_data = jd_reject_components(data, comp, cfg)
% this function combines ft_reject_component with a convenient user
% interface. See video tutorial for an explanation of the functionality.
%
% ******************CHANGELOG******************
% created:      -23/12/2020-
%               -21-01-2021-
% function can now handle datasets with varying trial lengths
%               -23-02-2021-
% added ability to remove all components that load equivalently aross
% channels

% the info structure will be attached to the figure
% and passed around between the callback functions

% incorporate input data into info structure to avoid double data
info.data.data = data; clear data
info.data.comp = comp; clear comp

% setup figure
channels = length(info.data.data.label);
[r,c] = partition(channels,2);
f = figure('units','normalized','outerposition',[0.2 0.2 0.8 0.8]); % ,'menu','none');
left = 0.02;
bottom = 0.53;
width = 0.88;
height = 0.45;
w = width/c;
h = height/r;
[l, b] = meshgrid(left:w:left+(width-w), bottom:h:bottom+(height-h));
l = l';
b = flipud(b)';
% channel trace axes
limit = cfg.alim;
chanNums = cell2mat(cellfun(@(x) str2double(regexp(x,'\d*','match')),info.data.data.label, 'UniformOutput', false));
[~, chanOrder] = sort(chanNums);
for idx = 1:channels
    info.ax(idx) = axes(f,'Position',[l(idx),b(idx),w,h]);
    set(info.ax(idx),'XTick',[],'XTickLabel',[],'YTick',[],'YTickLabel',[], ...
    'FontSize',6,'YLim',[-limit/2,limit/2]);
    info.ax(idx).Title.String = info.data.data.label{chanOrder(idx)};
    info.ax(idx).NextPlot = 'add';
end
hr = 1-bottom;
ratio = 0.7;
% mean trace axes
info.ax(idx+1) = axes(f,'Position',[left,0.04+hr*ratio,width,hr*(1-ratio)]);
info.ax(idx+1).YLabel.String = 'mean signal';
set(info.ax(end-1),'XTickLabel',[],'YTick',[],'YTickLabel',[],'YLim',[-limit/2,limit/2]);
info.ax(idx+1).NextPlot = 'add';
% components axes
info.ax(idx+2) = axes(f,'Position',[left,0.04,width,hr*ratio]);
info.ax(idx+2).YAxisLocation = 'right';
info.ax(idx+2).Interactions = zoomInteraction('Dimensions','y');
info.ax(idx+2).XLabel.String = 'time (s)';
info.ax(idx+2).YLabel.String = 'component';
info.ax(idx+2).NextPlot = 'add';
linkaxes([info.ax(idx+1),info.ax(idx+2)],'x');

% init other info fields
info.rejflag = cell2mat(cellfun(@(x) all(isnan(x),'all'),info.data.data.trial,'UniformOutput',false));
if isfield(info.data.data,'origData') % this is the case if the data has already gone through this function before
    if all(cfg.removeUnivalentComps) %if true or nonzero number(s)
        [info.icmp,info.data.clean] = pre_reject(info.data.data, info.data.comp, cfg.removeUnivalentComps);
    else 
        info.icmp = info.data.data.rmcomps;
        info.data.clean = info.data.data.trial;
    end
    info.data.data.trial = info.data.data.origData;
    info.origNaN = cell2mat(cellfun(@(x) ~all(isnan(x),'all'),info.data.data.origData,'UniformOutput',false));
else
    % remove components that load with similar valence across all channels
    if all(cfg.removeUnivalentComps) %if true or nonzero number(s)
        [info.icmp,info.data.clean] = pre_reject(info.data.data, info.data.comp, cfg.removeUnivalentComps);
    else 
        info.icmp = false(length(info.data.comp.label),length(info.data.comp.trial));
        info.data.clean = info.data.data.trial;
    end
    info.origNaN = cell2mat(cellfun(@(x) ~all(isnan(x),'all'),info.data.data.trial,'UniformOutput',false));
end
info.chanNums = chanNums;
info.Nchan = numel(info.data.data.label);
info.chanOrd = chanOrder;
info.cmp = info.data.comp.label;
info.trls = 1:length(info.data.data.trial);
info.itrl = find(~info.rejflag,1);
info.newTrl = true;
info.ylim = limit;
info.quit = false;
info.reset = false;
info.cmpflag = true;
info.eqflag = true;

% setup controls
[r,c] = partition(length(info.data.comp.label),0.5);
[bx, by] = meshgrid(linspace(max(l,[],'all')+w+0.01,0.99,c+1), ...
                    linspace(min(b,[],'all')+h,0.96,r+1));
butSiz = [abs(bx(1)-bx(end))/c, abs(by(1)-by(end))/r];
bx = bx(1:end-1,1:end-1)';
by = flipud(by(1:end-1,1:end-1))';
% butSiz = [0.02 0.02];
% [bx, by] = meshgrid(-butSiz(1)*(c/2):butSiz(1)+0.001:butSiz(1)*(c/2),-butSiz(2)*(r/2):butSiz(2)+0.001:butSiz(2)*(r/2));
% bx = flip(bx+left+width+(1-(left+width))/2);
% by = flip(by+bottom+height/2);
info.ui.msgCmp      = uicontrol(f,'units','normalized','position',[bx(1) by(1)+butSiz(2) butSiz(1)*c 0.02],'String','toggle components:','Style','text');
for idx = 1:channels
    info.ui.setCmp(idx) = uicontrol(f,'units','normalized','position',[bx(idx) by(idx) butSiz(1) butSiz(2)], ...
                          'String',num2str(idx),'Callback',@setCmp, 'Style','togglebutton');
end
info.ui.Delta       = uicontrol(f,'units','normalized','position',[0.96 0.57 0.03 0.03],'String','Delta','Style','checkbox');
info.ui.topo        = uicontrol(f,'units','normalized','position',[0.93 0.52 0.03 0.05],'String','mapping','Callback',@topo);
info.ui.spectral    = uicontrol(f,'units','normalized','position',[0.96 0.52 0.03 0.05],'String','inspectral','Callback',@spectral);
info.ui.msgGen      = uicontrol(f,'units','normalized','position',[0.93 0.405 0.06 0.10],'String','standby','FontSize',14,'FontWeight','bold','Style','text');
info.ui.msgTrl      = uicontrol(f,'units','normalized','position',[0.93 0.40 0.06 0.02],'String','trial settings:','Style','text');
info.ui.rejectTrl   = uicontrol(f,'units','normalized','position',[0.93 0.35 0.06 0.05],'String','reject trial','Callback',@reject);
info.ui.setTrlNext  = uicontrol(f,'units','normalized','position',[0.96 0.31 0.03 0.03],'String','>','Callback',@next);
info.ui.setTrlPrev  = uicontrol(f,'units','normalized','position',[0.93 0.31 0.03 0.03],'String','<','Callback',@prev);
info.ui.setTrlNext10= uicontrol(f,'units','normalized','position',[0.96 0.27 0.03 0.03],'String','>>>','Callback',@next10);
info.ui.setTrlPrev10= uicontrol(f,'units','normalized','position',[0.93 0.27 0.03 0.03],'String','<<<','Callback',@prev10);
info.ui.reset       = uicontrol(f,'units','normalized','position',[0.93 0.21 0.06 0.05],'String','reset','Callback',@reset);
info.ui.quit        = uicontrol(f,'units','normalized','position',[0.93 0.10 0.06 0.05],'String','close','Callback',@stop);

guidata(f,info);

% interactivity loop
interactive = 1;
while interactive && ishandle(f)
    redraw(f);
    info = guidata(f);
    if info.quit == 0
        uiwait;
    else
        clean_data = finalize(info); % fill all empty trials with original data
        delete(f);
    break
    end
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function redraw(f,~)
persistent currdat origdat compdat clipdat rng;
if ~ishandle(f)
  return
end
info = guidata(f);

% set data to plot
if info.newTrl
    origdat = info.data.data.trial{info.itrl};
    compdat = info.data.comp.trial{info.itrl}';
    rng     = range(compdat(:,:),1)./2;
    rng     = -cumsum(rng+circshift(rng,1));
    compdat = compdat+rng;
    clipdat = info.data.data.clip{info.itrl};
    info.time    = info.data.data.time{info.itrl};
end
if all(info.data.clean{info.itrl} == origdat, 'all') % (info.newTrl && isempty(info.data.clean{info.itrl})) || info.reset
    currdat = origdat;
    info.eqflag = true;
elseif info.reset
    currdat = origdat;
    info.data.clean{info.itrl} = origdat;
    info.eqflag = true;
else 
    currdat = squeeze(info.data.clean{info.itrl});
    info.eqflag = false;
end
if any(info.icmp(:,info.itrl) ~= [info.ui.setCmp(:).Value]')
    for idx = find(info.icmp(:,info.itrl) ~= [info.ui.setCmp(:).Value]')' % set buttons to last configuration
         info.ui.setCmp(idx).Value = info.icmp(idx,info.itrl);
    end
end
% % % if all(all(origdat == currdat))
% % % else
% % %     info.eqflag = false;
% % % end
% plot channels
for idx = 1:info.Nchan % plot channel data
    cla(info.ax(idx));
    if ~info.eqflag || info.rejflag(info.itrl)
        plot(info.ax(idx),origdat(info.chanOrd(idx),:),'color',[0,0,1,0.5],'LineStyle','--','LineWidth',0.2);
    end
    plot(info.ax(idx),currdat(info.chanOrd(idx),:),'color',[0,0,0,0.5]);
    plt = area(info.ax(idx),clipdat(info.chanOrd(idx),:).*(info.ylim*2)-info.ylim,-info.ylim,'FaceColor',[1 0 0]);
    plt.EdgeColor = 'none';
    plt.FaceAlpha = 0.1;
end
% plot channel mean
cla(info.ax(end-1));
if ~info.eqflag || info.rejflag(info.itrl)
    plot(info.ax(end-1),info.time,mean(origdat,1,'omitnan'),'color',[0,0,1,0.5],'LineWidth',0.2,'DisplayName','original');
end
plot(info.ax(end-1),info.time,mean(currdat,1,'omitnan'),'color',[0,0,0,0.5],'DisplayName','current');
legend(info.ax(end-1),'Location','NorthEast');
% plot comp data
if (info.cmpflag || info.reset) && ~all(isnan(compdat), 'all')
    cla(info.ax(end));
    for idx = 1:length(info.cmp)
        plt = plot(info.ax(end),info.time,compdat(:,idx));
        if info.icmp(idx,info.itrl)
            plt.Color = [0 0 0 0.2];
        end
    end
    info.ax(end).XLim = [info.time(1), info.time(end)];
    info.ax(end).YTick = flip(rng);
    info.ax(end).YTickLabel = flipud(num2cell(1:length(info.cmp))');
    if info.newTrl
        info.ax(end).YLim = [rng(end)-0.1*range(rng), rng(1)+0.1*range(rng)];
    end
elseif all(isnan(compdat))
    cla(info.ax(end));    
end
% reset flags
info.ui.msgGen.String = sprintf('trial: \n%i/%i',info.itrl,numel(info.trls));
info.newTrl = false;
info.cmpflag = false;
info.reset = false;
guidata(f,info);
drawnow
end

function [r,c] = partition(N,ratio)
opts = find(mod(N./(1:N),1) == 0);
while numel(opts) <= 4 % make sure there is a decent row-column ratio possible
    N = N+1;
    opts = find(mod(N./(1:N),1) == 0);
end
ridx = dsearchn((opts./flip(opts))', ratio);
c = opts(ridx);
r = N/c;
end

function [icmp, data] = pre_reject(data,comp, rm)
% this function removes al components from the data that have similar
% valence across channels.
if islogical(rm) 
    icmp = repmat(all(comp.topo > 0, 1)',1,length(comp.trial));
else % if comp is vector of numbers
    icmp = false(numel(comp.label),length(comp.trial));
    icmp(rm,:) = true;
end
if isfield(data,'origData')
    trials = find(any(icmp ~= data.rmcomps,1));
    icmp(data.rmcomps) = true;
    mockdat = data;
    mockcomp = comp;
    for itrl = trials
        cfg = [];
        cfg.component = find(icmp(:,itrl));
        mockdat.trial = data.origData(itrl);
        mockdat.time = data.time(itrl);
        mockcomp.trial = comp.trial(itrl);
        mockcomp.time = comp.time(itrl);
        tmp = ft_rejectcomponent(cfg,mockcomp,mockdat);
        data.trial(itrl) = tmp.trial;
    end
elseif ~isfield(data,'origData')
    cfg.component = find(icmp(:,1));
    data = ft_rejectcomponent(cfg,comp,data);
end
data = data.trial;
end

function setCmp(f,~)
info = guidata(f);
for idx = 1:numel(info.cmp)
    info.icmp(idx,info.itrl) = logical(info.ui.setCmp(idx).Value);
end
% prep input arguments
if all(~info.icmp(:,info.itrl)) % if there are no rejected components -> clean = original
    info.data.clean(info.itrl) = info.data.data.trial(info.itrl);
else
    cfg = [];
    cfg.component = find(info.icmp(:,info.itrl));
%     cfg.demean = 'no';
    comp = rmfield(info.data.comp,'trial'); comp.trial = info.data.comp.trial(info.itrl); comp.time = {info.time};
    data = rmfield(info.data.data,'trial'); data.trial = info.data.data.trial(info.itrl); data.time = {info.time};
    % partial out components and store result
    clean = ft_rejectcomponent(cfg,comp,data);
    info.data.clean(info.itrl) = clean.trial; % {clean.trial{1} ./ (2*info.limit)};
    info.reset = false;
end
info.rejflag(info.itrl) = false;
info.cmpflag = true;
guidata(f,info);
uiresume;
end

function reject(f,~)
info = guidata(f);
info.data.clean{info.itrl}(:,:) = NaN;
info.rejflag(info.itrl) = true;
guidata(f,info);
uiresume;
end

function reset(f,~)
info = guidata(f);
if info.rejflag(info.itrl)
    setCmp(f);
else
    info.reset = true;
    info.icmp(:,info.itrl) = false;
    guidata(f,info);
    uiresume;
end
end

function next(f,~)
info = guidata(f);
shift = find(info.origNaN(info.itrl+1:end),1);
if ~isempty(shift)
    info.itrl = info.itrl+shift;
    info.newTrl = true;
    info.cmpflag = true;
else
    msgbox('you have reached the last valid trial.');
end
guidata(f,info);
uiresume;
end

function prev(f,~)
info = guidata(f);
shift = find(flip(info.origNaN(1:info.itrl-1)),1);
if ~isempty(shift)
    info.itrl = info.itrl-shift;
    info.cmpflag = true;
    info.newTrl = true;
else
    msgbox('you have reached the first valid trial.');
end
guidata(f,info)
uiresume;
end

function next10(f,~)
info = guidata(f);
if info.itrl < numel(info.trls)+10
    info.itrl = info.itrl+10;
    info.cmpflag = true;
    info.newTrl = true;
elseif info.itrl == numel(info.itrls)
    msgbox('you have reached the last valid trial.');
else
    info.cmpflag = true;
    info.newTrl = true;
    info.trl = numel(info.trls);
end
guidata(f,info);
uiresume;
end

function prev10(f,~)
info = guidata(f);
if info.itrl > 10
    info.itrl = info.itrl-10;
    info.cmpflag = true;
    info.newTrl = true;
elseif info.itrl == 1
    msgbox('you have reached the first valid trial.');
else
    info.newTrl = true;
    info.cmpflag = true;
    info.itrl = 1;
end
guidata(f,info);
uiresume;
end

function spectral(f,~)
info = guidata(f);
CSC = inputdlg('select CSC',' ',1);
CSC = str2double(CSC{1});
if isnan(CSC) 
    warndlg('you did not select a channel.');
elseif ~any(ismember(CSC,info.chanNums))
    warndlg(sprintf('CSC%d does not exist. Perhaps you deleted it',CSC));
else
    chi = find(info.chanNums == CSC); % channel index of corresponding CSC
    fs = info.data.data.fsample;
    nyquist = fs/2;
    freqres = fs/numel(info.time);
    frequency = 0:freqres:nyquist;
    nfrq = numel(frequency);
    lim = [info.time(1),info.time(end)];
    tick = linspace(lim(1),lim(2),11);
%     ticklab = num2cell(linspace(lim(1),lim(2),11))';

    figure;
    % time
    ax1 = subplot(3,2,1);
    plot(ax1,info.time,info.data.data.trial{info.itrl}(chi,:));
    title('original data');
    xlabel('time');
    ylabel('potential (\muV)');
    set(ax1,{'XLim','XTick'},{lim,tick});
    grid on
    ax2 = subplot(3,2,2);
    plot(ax2,info.time,info.data.clean{info.itrl}(chi,:));
    title('alternative data');
    xlabel('time (s)');
    ylabel('potential (\muV)');
    set(ax2,{'XLim','XTick'},{lim,tick});
    grid on
    
    % time-frequency
    window = 1*fs;
    overlap = round(0.9*fs);
    ax3 = subplot(3,2,3);
    loc3 = get(gca, 'Position');
    [~,~,time,~] = spectrogram(info.data.data.trial{info.itrl}(chi,:),blackman(window),overlap,[],fs,'yaxis','MinThreshold',0);
    time = time+lim(1);
    [s3,f,~,p3] = spectrogram(info.data.data.trial{info.itrl}(chi,:),blackman(window),overlap,[],'yaxis','MinThreshold',0);
    pcolor(ax3,time,f.*nyquist,10*log10(p3)); shading flat; grid on;
    ax3.XLabel.String = 'time (s)';
    ax3.YLabel.String = 'freq (Hz)';
    ax4 = subplot(3,2,4);
    loc4 = get(gca, 'Position');
    try
        [s4,f,~,p4] = spectrogram(info.data.clean{info.itrl}(chi,:),blackman(window),overlap,[],'yaxis','MinThreshold',0);
    catch % happens if trial has been rejected
        s4 = s3;
        p4 = p3;
    end
    if info.ui.Delta.Value
        dBChange = 10*log10((abs(s4)./abs(s3)).^2);
%         pcntChange = 100.*(abs(s4)-abs(s3))./abs(s3); set(ax4,'CLim',[-100,100]);
        pcolor(ax4,time,f.*nyquist,dBChange); shading flat; grid on;
        c = colorbar(ax3); c.Label.String = 'power spectral density';
        c = colorbar(ax4); c.Label.String = '\Delta power (dB)';
%         pcolor(ax4,time,f,p4-p3); shading flat; grid on;
%         c = colorbar;(ax4); c.Label.String = '\Delta power spectral density';
        linkaxes([ax3,ax4],'x');
    else
        pcolor(ax4,time,f.*nyquist,10*log10(p4)); shading flat; grid on;
        % match color scaling
        cmax = max([ax3.CLim(2); ax4.CLim(2)]);
        ctick = {0:10:cmax};
        ax3.CLim = [0 cmax]; c = colorbar(ax3);
        c.Label.String = 'power spectral density'; % if you prefer power, add 'power' to spectrogram input
        ax4.CLim = [0 cmax]; c = colorbar(ax4);
        c.Label.String = 'power spectral density'; % if you prefer power, add 'power' to spectrogram input
        linkaxes([ax3,ax4],'xy');
    end
    set(ax3,{'Position','XLim','XTick','YScale'},{loc3,lim,tick,'log'});
    set(ax4,{'Position','XLim','XTick','YScale'},{loc4,lim,tick,'log'});
    ax4.XLabel.String = 'time (s)';
    ax4.YLabel.String = 'freq (Hz)';
   
    % frequency
    ax5 = subplot(3,2,5);
    freq1 = fft(info.data.data.trial{info.itrl}(chi,:));
    plot(frequency,movmean(10*log10(abs(freq1(1:nfrq)).^2),5)); % plot a running average to facilitate inspection
    xlabel('freq');
    ylabel('power (dB)');
    xlim([0,150]);
    grid on
    ax6 = subplot(3,2,6);
    freq2 = fft(info.data.clean{info.itrl}(chi,:));
    if info.ui.Delta.Value
        dBChange = 10*log10((abs(freq2)./abs(freq1)).^2);
        plot(frequency,movmean(dBChange,20));        
        ylabel('\Delta power (dB)');
        linkaxes([ax5,ax6],'x');
    else
        plot(frequency,movmean(10*log10(abs(freq2(1:nfrq)).^2),5));
        ylabel('power (dB)');
        linkaxes([ax5,ax6],'xy');
    end
    xlabel('freq (Hz)');
    grid on
    
    % link axes
    linkaxes([ax1,ax2],'y');
    linkaxes([ax1,ax3],'x');
    linkaxes([ax2,ax4],'x');
end
uiresume;
end

function topo(f,~)
info = guidata(f);
figure;
heatmap(normalize(info.data.comp.topo(info.chanOrd,:)',2,'norm'));
set(gca,'XDisplayLabels',info.chanNums(info.chanOrd));
title('component weight mapping (normalized)');
ylabel('component');
xlabel('CSC');
clr = [repelem(255,256/2),255:-2:0];
colormap(uint8([flip(clr);clr;repelem(255,256)]'));
% c.Label.String = 'weight (normalized)';
uiresume;
end

function stop(f,~)
info = guidata(f);
info.quit = 1;
guidata(f,info);
uiresume;
end

function out = finalize(info)
% swap original data for clean data , delete rejected trials
if ~isfield(info.data.data,'origData')
    info.data.data.origData = info.data.data.trial;
end
% fill empty trials with original data
empty_trials = find(cellfun('isempty', info.data.clean)); % rejected trials are not empty (they are NaN matrices)
info.data.clean(empty_trials) = info.data.data.trial(empty_trials);
info.data.data.trial = info.data.clean;
% log changes (removed components, ...)
info.data.data.rmcomps = info.icmp;
% note rejected trials in rmtrls
% if isfield(info.data.data,'rmtrls') % occurs if you ran jd_reject_clippers or re-ran this function
%     % compensate for earlier loss of trials (in jd_reject_clippers)
%     left = find(~info.data.data.rmtrls);
%     info.data.data.rmtrls(left(info.rejflag)) = true;
% else
%     info.data.data.rmtrls = info.rejflag;
% end
out = info.data.data;
end
