function Data = jd_reject_clippers(Data)
% this function allows you to reject trials based on clipping statistics
%
% ******************CHANGELOG******************
% created:      -04/01/2021-
%               -21-01-2021-
% function can now handle datasets with varying trial lengths

% prepare data
clipdat = cell2mat(cellfun(@(x) mean(x,2),Data.clip, 'UniformOutput',false));
% clipdat = cat(3,Data.clip{:});
% clipdat = squeeze(mean(clipdat,2));
srt = [sum(clipdat ~= 0,1)', sum(clipdat,1)']; % define sort criteria [Nclippers,cumulativeClipSeverity]
[~, order] = sortrows(srt,[1,2],'descend');
order = order';
% [~,order] = sort(sum(clipdat ~= 0,1),'descend'); % find order from most to least number of clipping channels
% [~,order] = sort(sum(clipdat,1),'descend'); % find order from most to least cumulative clipping
info.plotdat = clipdat(:,order); % order trials accordingly
info.plotdat = info.plotdat(:,sum(info.plotdat,1) ~= 0); % don't plot trials with zero cumulative clipping

f = figure;

% setup info structure
info.update = uicontrol(f,'units','normalized','position',[0.93 0.35 0.06 0.05],'String','update','Callback',@update);
info.msg    = uicontrol(f,'units','normalized','position',[0.93 0.28 0.06 0.05], ...
              'String','remove channels:       (use : or ,)','Style','text');
info.response   = uicontrol(f,'units','normalized','position',[0.93 0.25 0.06 0.05],'Style','edit');
info.msg2   = uicontrol(f,'units','normalized','position',[0.93 0.18 0.06 0.05], ...
              'String','remove trials:       (use : or ,)','Style','text');
info.response2   = uicontrol(f,'units','normalized','position',[0.93 0.15 0.06 0.05],'Style','edit');
info.select      = uicontrol(f,'units','normalized','position',[0.93 0.10 0.06 0.05],'String','OK','Callback',@reject);
info.rmChans = [];
info.rmTrls = [];
info.quit = false;

guidata(f,info);

interactive = 1;
while interactive && ishandle(f)
    redraw(f);
    info = guidata(f);
    if info.quit == 0
        uiwait;
    else
        if isempty(info.rmTrls)
            fprintf('\nno trials were set to NaN\n\n');
        else
%             % remove rejected trials
%             Data.rmtrls = false(numel(Data.trial),1);
%             Data.rmtrls(order(info.rmTrls)) = true; % keep a record of deleted trial numbers
            for ri = order(info.rmTrls) % set all rejected trials to NaN
                Data.trial{ri}(:,:) = NaN;
                Data.clip{ri}(:,:) = false;
            end
%             Data.trial = Data.trial(~Data.rmtrls);
%             Data.time = Data.time(~Data.rmtrls);
%             Data.clip = Data.clip(~Data.rmtrls);
            fprintf('\ntrials set to NaN: \n%s\n\n',num2str(sort(order(info.rmTrls))));
        end
        if isempty(info.rmChans)
            fprintf('\nno channels were deleted\n\n');
        else            
            % remove rejected channels
            keepChans = true(numel(Data.label),1);
            keepChans(info.rmChans) = false;
            Data.trial = cellfun(@(x) x(keepChans,:),Data.trial, 'UniformOutput', false);
            Data.label = Data.label(keepChans);
            Data.clip = cellfun(@(x) x(keepChans,:),Data.clip, 'UniformOutput', false);
            fprintf('\ndeleted channels in index: \n%s\n\n',num2str(info.rmChans));
        end
        delete(f);
        interactive = 0;
%     break
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function redraw(f,~)
info = guidata(f);
% remove selected trials and channels
curdat = info.plotdat;
try
    curdat(:,info.rmTrls) = 0;
end
try
    curdat(info.rmChans,:) = 0;
end

% plot
ax = heatmap(curdat);
colormap('turbo');
set(ax,'ColorLimits',[0,1]);
title('proportion of time clipping');
xlabel('trials with clipping (sorted)');
ylabel('channel index');
guidata(f,info);
drawnow;
end

function update(f,~)
info = guidata(f);
info.rmChans = str2num(info.response.String);
info.rmTrls = str2num(info.response2.String);
guidata(f,info);
uiresume;
end

function reject(f,~)
info = guidata(f);
info.quit = true;
info.rmChans = str2num(info.response.String);
info.rmTrls = str2num(info.response2.String);
guidata(f,info);
uiresume;
end
