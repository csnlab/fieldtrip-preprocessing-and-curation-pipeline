function jd_tf_3d(cfg, data)
% currently only works with cfg.method = 'fourier'

% setup figure
h = figure;
axis([0 1 0 0.8]);
axis off

% the info structure will be attached to the figure
% and passed around between the callback functions
info.ax = axes(h);
info.data = data.fourierspctrm;
info.freq = data.freq;
info.ifreq = 1;
info.trl = 1:size(data.trialinfo,1);
info.itrl = info.trl;
info.chan = data.label;
info.ichan = 1;
info.time = data.time';
info.itime = 1:numel(data.time);
info.quit = 0;
info.feature = ["phase","power","phase_progression"]; % sets color
info.ifeature = info.feature(1);

info.ui.msgFreq     = uicontrol(h,'units','normalized','position',[0.92 0.83 0.07 0.03],'String','color:', ...
                    'Style','text');
info.ui.setFeature  = uicontrol(h,'units','normalized','position',[0.92 0.78 0.07 0.05],'String',info.feature, ...
                    'Callback',@setFeature,'Style','popupmenu');
info.ui.msgFreq     = uicontrol(h,'units','normalized','position',[0.92 0.74 0.07 0.03],'String','frequency:', ...
                    'Style','text');
info.ui.setFreq     = uicontrol(h,'units','normalized','position',[0.92 0.69 0.07 0.05],'String',info.freq, ...
                    'Callback',@setFreq,'Style','popupmenu');
info.ui.msgChan     = uicontrol(h,'units','normalized','position',[0.92 0.65 0.07 0.03],'String','channel:', ...
                    'Style','text');
info.ui.setChan     = uicontrol(h,'units','normalized','position',[0.92 0.6 0.07 0.05],'String',info.chan, ...
                    'Callback',@setChan,'Style','popupmenu');
info.ui.msgTrl     = uicontrol(h,'units','normalized','position',[0.92 0.552 0.07 0.03],'String','trials:', ...
                    'Style','text');
info.ui.setTrl      = uicontrol(h,'units','normalized','position',[0.92 0.201 0.07 0.35],'String',info.trl, ...
                    'Callback',@setTrl,'Style','listbox','Min',1,'Max',length(info.trl));
info.ui.select        = uicontrol(h,'units','normalized','position',[0.92 0.15 0.07 0.05 ],'String','select','Callback',@select);
                % info.ui.setFeature
info.ui.quit        = uicontrol(h,'units','normalized','position',[0.92 0.05 0.07 0.05 ],'String','quit','Callback',@stop);
% set(gcf, 'WindowButtonUpFcn', @button);
% set(gcf, 'KeyPressFcn', @key);

guidata(h,info);

% interactivity loop
interactive = 1;
while interactive && ishandle(h)
  redraw(h);
  info = guidata(h);
  if info.quit == 0
    uiwait; %(h)
  else
%     chansel = info.chansel;
%     trlsel = info.trlsel;
    delete(h);
    break
  end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function redraw(h)
if ~ishandle(h)
  return
end
info = guidata(h);
cla;
% set data to plot
plotdat = info.data(info.itrl,info.ichan,info.ifreq,info.itime);
plotdat = squeeze(plotdat)'; % take average across trials and remove singleton dims (chan and freq)
% plotdat = permute(plotdat,[2,1]); % prepare for plotting
switch info.ifeature %
    case "power"
        clr = log(abs(plotdat).^2);
        cmp = 'jet';
        clim = [0,max(clr,[],'all')*0.95];
%         set(info.ax,'ColorScale','log')
        clab = "power (dB)";
    case "phase"
        clr = angle(plotdat);
        cmp = 'hsv';
        clim = [-pi,pi];
        clab = "phase (rad.)";
    case "phase_progression"
        clr = diff(angle([plotdat(1,:);plotdat]));
        clr(clr < 0) = clr(clr < 0)+2*pi;
        cmp = 'jet';
        clim = [min(clr,[],'all'),max(clr,[],'all')];
        clab = "phase progression (rad^-1)";
end
% plot and format
% width = [zeros(size(info.time)), ones(size(info.time))];
% hg = gobjects(size(info.itrl));
% for Ti = 1:info.itrl-50 % ribbon plot with increased functionality
%     hg(Ti) = surface([info.time,info.time],width+Ti,abs(repmat(plotdat(:,Ti),1,2)).^2,repmat(clr(:,Ti),1,2),info.ax); %abs^2 for power
% end
jd_ribbon(info.ax,info.time,abs(plotdat).^2,clr);
% surf(info.ax,info.time,info.itrl,abs(plotdat).^2,clr); %abs^2 for power
colormap(cmp);
h.Children(end).Position = [0.13 0.11 0.68 0.815];
info.ax.Position = [0.13 0.11 0.68 0.815];
% shading interp
title(sprintf('channel %s @ %.2f Hz', info.chan{info.ichan}, info.freq(info.ifreq)));
xlabel('time (s)');
ylabel('trial#');
zlabel('power');
caxis(clim); % rescale colorbar
c = colorbar;
c.Label.String = clab;
drawnow
end

function setFeature(h,~)
info = guidata(h);
info.ifeature = info.ui.setFeature.String{info.ui.setFeature.Value};
guidata(h,info);
uiresume;
end

function setFreq(h,~)
info = guidata(h);
info.ifreq = info.ui.setFreq.Value;
guidata(h,info);
uiresume;
end

function setChan(h,~)
info = guidata(h);
info.ichan = info.ui.setChan.Value;
guidata(h,info);
uiresume;
end

function setTrl(h,~)
info = guidata(h);
info.itrl = info.trl(info.ui.setTrl.Value);
guidata(h,info)
uiwait;
info.ui.select = 0;
uiresume;
end

function select(h,~)
info = guidata(h);
if numel(info.ui.setTrl.Value) <= 1
    msgbox('select at least 2 trials');
    return
end
uiresume;
end

function stop(h,~)
info = guidata(h);
info.quit = 1;
guidata(h,info)
uiresume;
end

function hh = jd_ribbon(cax,x,y,c)
% MATLAB ribbon function with enhanced color functionality
cax = newplot(cax);
nextPlot = cax.NextPlot;

zz = [zeros(length(x),1), ones(length(x),1)];

n = size(y,2);
h = gobjects(n,1);
for n=1:size(y,2)
    cc = repmat(c(:,n),1,2);
    h(n) = surface([x x],zz+n,[y(:,n) y(:,n)],cc,'parent',cax, 'EdgeColor','interp','FaceColor','interp');
end

switch nextPlot
    case {'replaceall','replace'}
        view(cax,3);
        grid(cax,'on');
    case {'replacechildren'}
        view(cax,3);
end

if nargout>0, hh = h; end
end
