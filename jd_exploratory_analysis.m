% this script lets you perform multiple analyses to assess the results of
% your artefact rejection. It does not split the data in to conditions on
% purpose, because I don't want you to select your analysis based on
% how it relates to your hypotheses. To start, load fieldtrip and load
% a preprocessed dataset and use the first sections to find a baseline
% window that you're comfortable with. After that you can run sections in
% any order you want.

% to do:
% - create ability to use different kernel methods based on cut-off
% - allow user to choose for ITI baselines in addition to baseline trials
% (use ft functionality for this)
% - enable processing with baselinetrials boolean vector
% frequency
%% load fieldtrip
addpath 'C:\Program Files\MATLAB\R2018a\toolbox\fieldtrip-20191028';
ft_defaults

%% load preprocessed data
root = "C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\concentric circles";
addpath(genpath('C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\fieldtrip pipeline'));
dataDir = fullfile(root, 'processed_data');

oldDir = cd(dataDir); % choose data folder as current directory
session = uigetfile; % user-friendly way to load filenames
load(session); % file was previously named 'processedData'

%% inspect baseline trials and ITIs
% you must find out what pre-stimulus period you can use as a baseline.
% This in turn detemines the minimum frequency that can use this period as
% a baseline. All frequencies below that baseline must instead use the
% baseline trials.

% note: extreme values in the baseline timeseries is not very problematic because
% you will use the mean of te time series to perform baseline correction. 
% tsp contains time windows as: [start baseline, end baseline, start ITI, end ITI]

tsp = jd_set_baselines(curatedData);
%% compare baseline trials to ITI
% next you want to see how the power spectrum from baseline trials compares
% to those in the ITI.
% after this and previous section you should be able to determine if you want to use
% trial or ITI baselines, and if you want to use trial baseline averaging
% whether you prefer the mean or median.

% in both the trial and baseline figures, look out for extreme
% outliers within each trial, especially if they cluster in a specific
% frequency band. If you find such an outlier consider doing a
% bootstrapping analysis to determine its influence. 
% trials that are approximately 1/f but flat along the spectrum are also
% suspicious.

TrialBaselineWindow = tsp(1:2); %base this window on baselinedurations and your observations from the previous plots
ITIBaselineWindow   = tsp(3:4); % be sure to check that range is within ITI

compare_baselines(curatedData, channel, TrialBaselineWindow, ITIBaselineWindow);

% repeat this section for different channels

%% determine your filter method and take a look at the filterbank
% now that you know your baseline it is time to select your kernel family
% Based on those two you can see the minimum frequency for which you can use
% the ITI baseline and decide how you want to proceed

% here you can tweak your filter parameters. the cfg output will carry
% these to subsequent fieldtrip functions

% general kernel parameters
cfg.method = 'wavelet'; % 'hilbert' 'wavelet' 'mtmconvol'
cfg.lowfreq = 10;
cfg.highfreq = 80;
cfg.numfreq = 20;
cfg.freqscale = 'linear'; % this refers to center freqs of kernels, not scaling of y-axis.

% wavelet-specific parameters
cfg.width = linspace(3,30,cfg.numfreq); % number of cycles in wavelet (determines tf trade-off)
cfg.gwidth = 4; % length of the wavelets in std (determines cut-off of gaussian)

% Hilbert-specific parameters (band-pass)
% note: this script uses the firls filter function, which contains bugs in
% fieldtrip (see bug 2453)
cfg.transition_width = 0.2; % use value between 0.1-0.25 (mike cohen)
cfg.order = 2; % maximum kernel width in seconds. This variable controls filter order. higher filter order means better freq precision but poorer temporal precision

% mtmconvol-specific parameters
cfg.taper = 'dpss'; % 'dpss' | 'hanning' | more options in fieldtrip but not here
cfg.tapsmofrq =  ones(cfg.numfreq,1)* 3; % logspace(log10(5),log10(10),cfg.numfreq); % 1 x numfoi, half the amount of spectral smoothing
cfg.factor = linspace(1,1.7,cfg.numfreq)'; % multiplication of absolute minimum kernel width (which would allow only 1 slepian taper)

cfg = inspect_kernels(curatedData, cfg);

%% calculate cut-off frequency and set standard variables
% standard or frequency-relative time windows? consult MikeXCohen and other
% literature

% calculate cut-off (keep in mind kernel width)

%% get time-frequency power spectrum and perform baseline correction
% we will do this for all channels at once
% baseline and trials will be transformed using identical kernels
cfg. method = cfg.method; % 'mtmconvol' | 'wavelet' | 'hilbert'
cfg.output = 'pow'; % 'pow' | 'powandcsd' | 'fourier'
cfg.channel = 'all';
cfg.trials = 'all' ; % the function will distinguish between baselinetrials and stimulus trials
cfg.keeptrials = 'yes'; % keep individual trials or return average
cfg.keeptapers = 'no'; % return individual tapers or average
cfg.pad = 'nextpow2'; % pads the ends of the time bin sequence (0000[bin][bin][bin][bin]0000)
cfg.padtype = 'zero'; % 'zero', 'mean', 'localmean', 'edge', 'mirror', 'nan' or 'remove'
cfg.polyremoval = 0;

% set time bins of interest
time_resolution = 0.01; % (s)
nbltrl = find(~curatedData.sessionData.baselinetrials,1); % find duration of non-baseline trials
cfg.toi = curatedData.time{nbltrl}(1):time_resolution:curatedData.time{nbltrl}(end);

% baseline parameters
cfg.averagebltrials = 'mean'; % 'no' 'mean' or 'median' 
cfg.contrast = 'relchange'; % relchange = %change from baseline. See baseline function for alternatives
[cfg, tf_curatedData] = jd_freqanalysis(cfg, curatedData, tsp);

%% save (temporary)
tfPath = 'C:\Users\J.M. van Daatselaar\Documents\PhD\Bosman Vittini\projects\concentric circles\processed_data\TF';
save(fullfile(tfPath,session),'tf_curatedData', '-v7.3');

%% explore data in time-frequency domain
jd_tf_3d(cfg, tf_curatedData);

%% signal to noise
% Now that we have found the right baseline window and type for the data we
% can inspect the SNR. This approach was taken form Mike X Cohen (ch 18.8).

compute_snr(cfg, tf_curatedData);
%% time-frequency power spectrum
baselineTime = [4.2 4.5]; % in s
channels = {'CSC5'; 'CSC17'}; % optional
plotTimeFreq(curatedData, baselineTime, channels);

% %% power per trial (timeseries)
% % this function is convenient for finding outlying trials in the power spectrum
% % optionally, you can get the power data as output
% baselineTime = [curatedData.time{1}(1) curatedData.time{1}(end)]; % in s
% freq = 20; % frequency of interest
% channels = {'CSC5'; 'CSC17'}; 

power_per_trial(curatedData, baselineTime, freq, channels); % optional output param


%% get ERP per condition per session

CC_ERP = struct;

dirc = struct2cell(dir(dataDir));
dirci = contains(dirc(1,:), '_cur.mat');
filenames = dirc(1,dirci);

for si = 1:numel(filenames)
    % load file
    load(fullfile(dataDir,filenames(si)));
    % get animal ID
    % make booleans for conditions
    % calculate ERP (per condition) (mean & std)
    ERP = mean(cat(3,curatedData.trial{:}),3);
    % store results in structure
    % clear dataset
end























%% FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function cfg = inspect_kernels(Data,cfg)
Fs = Data.fsample;
nsamp = length(Data.trial{find(~Data.sessionData.baselinetrials,1)});
nyquist = Fs/2;
% Fs = 256;
time = linspace(-nsamp/Fs,nsamp/Fs,nsamp); % -nsamp/Fs/2:1/Fs:nsamp/Fs/2;
if strcmp(cfg.freqscale,'log')
    freqs = logspace(log10(cfg.lowfreq),log10(cfg.highfreq), cfg.numfreq);
elseif strcmp(cfg.freqscale,'linear')
    freqs = linspace(cfg.lowfreq, cfg.highfreq, cfg.numfreq);
else
    error('freq scale must be log or linear');
end
nFreq = length(freqs);
datTime = complex(zeros(nsamp,nFreq));
datFreq = complex(zeros(size(datTime)));
frequency = repmat(linspace(0,Fs,nsamp)',1,nFreq);
% frequency = repmat(linspace(0,Fs/2,round(nyquist)+1)',1,nFreq);
wavsamps = zeros(size(freqs));
lgd = cell(size(freqs));
switch cfg.method
    case 'wavelet'
        if numel(cfg.width) == 1
            cfg.width = repelem(cfg.width,nFreq);
        end
        for Fi=1:nFreq
            % construct wavelet
            [datTime(:,Fi), wavsamps(Fi)] = wavelet(cfg.method, Fs, freqs(Fi), cfg.width(Fi), cfg.gwidth, nsamp);
            datFreq(:,Fi)  = 10*log10(abs(fft(datTime(:,Fi))).^2);
            lgd{Fi} = sprintf('%.2f Hz | %.1f cycles',freqs(Fi),cfg.width(Fi));
        end
    case 'mtmconvol'
        warning(sprintf("time domain plot depicts max of multitaper sequence. width is correct.\n"));
        answer = questdlg('continue with max or min*factor taper width?','select','max','min','min');
        if strcmp(answer, 'max')
            wavsamps = ones(nFreq,1)*nsamp;
        else
            wavsamps = ceil((Fs./cfg.tapsmofrq).*cfg.factor);
        end
        for Fi = 1:nFreq
            [wvlt, ~]  = wavelet(cfg.taper, Fs, freqs(Fi), wavsamps(Fi), cfg.tapsmofrq(Fi), nsamp);
            wvltspctrm = fft(wvlt);
            datFreq(:,Fi) = 10*log10(mean(abs(wvltspctrm).^2,2));
            datTime(:,Fi) = max(real(wvlt),[],2);
%             datTime(:,Fi) = fftshift(ifft(datFreq(:,Fi))); % overestimates kernel width
            lgd{Fi} = sprintf('%.2f Hz | %d tapers',freqs(Fi),size(wvlt,2));
%             plot(real(wvlt)); % plot kernel sequence
        end
        cfg.t_ftimwin = wavsamps./Fs;
    case 'hilbert' %check if power is corectly calculated >> 10*log10(mean(abs(fft)).^2)
        if ~mod(length(time),2)
            time = time(1:end-1);
        end
        lt = length(time);
        wavsamps = ones(nFreq,1)*nsamp;
        order = Fs*cfg.order;
        while rem(order,4) ~= 0
            order = order-1;
        end
        idealresponse = [ 0 0 1 1 0 0 ];
        d = diff(freqs)/2;
        transFreqs = [freqs(1)-d(1), freqs+[d(1), d]];
        widths = (diff(transFreqs))/2;
        widths = [widths(1),widths];
        filters = [zeros(nFreq,1),transFreqs(1:end-1)'-widths(1:end-1)'*cfg.transition_width,transFreqs(1:end-1)',transFreqs(2:end)',transFreqs(2:end)'+widths(2:end)'*cfg.transition_width, repmat(nyquist,nFreq,1)]/nyquist;
        for Fi=1:nFreq
            assert(order<lt,sprintf('order exceeds max time window in iteration %d. Lower nCycles',Fi));
            tmpTime = firls(order, filters(Fi,:),idealresponse);
            tmpFreq = abs(fft(tmpTime));
            tmpfrequency  = linspace(0,Fs/2,ceil(length(tmpTime)/2));
            freqsidx = dsearchn(tmpfrequency',filters(Fi,:)'*(Fs/2));
            datTime(:,Fi) = padarray(tmpTime',(lt-length(tmpTime))/2,0,'both')';
            datFreq(:,Fi) = padarray(tmpFreq(1:length(tmpfrequency))',(lt-length(tmpfrequency)),tmpFreq(end),'post')';
            frequency(:,Fi) = padarray(tmpfrequency',(lt-length(tmpfrequency)),frequency(end),'post')';
            SSE = sum((idealresponse-tmpFreq(freqsidx)).^2 );
            lgd{Fi} = sprintf('%.2f Hz | SSE = %.3f',freqs(Fi),SSE);
        end
end
% plot results
figure;
subplot(2,1,1);
plot3(repmat(time',1,nFreq),real(datTime),ones(nsamp,1)*[1:nFreq]);
% shading interp
view([0,90]);
title(sprintf('%s family time domain',cfg.method));
xlabel('time (s)');
ylabel('amplitude');
set(gca,'xlim',[-max(wavsamps/Fs) max(wavsamps/Fs)]);


subplot(2,1,2);
plot(frequency,datFreq);
title(sprintf('%s family frequency domain',cfg.method));
xlabel('frequency (Hz)');
ylabel('Power');
set(gca,'xlim',[0 cfg.highfreq*1.2]);
legend(lgd,'Location',[0.85, 0.05, 0.2, 0.9]);

% add filter parameters to cfg
cfg.foi = freqs;
cfg.kernelN = wavsamps;
    function [wavout, acttapnumsmp] = wavelet(taper, Fs, freq, width, gwidth, nsample)
    switch taper
        case 'wavelet'
            dt = 1/Fs;
            sf = freq / width;
            st = 1/(2*pi*sf);
            toi2 = -gwidth*st:dt:gwidth*st;
            A = 1/sqrt(st*sqrt(pi));
            tap = (A*exp(-toi2.^2/(2*st^2)))';
        case 'dpss'
            % create a sequence of DPSS tapers, ensure that the input arguments are double precision
            tap = dpss(double(width), double(width .* (gwidth ./ Fs)))';
            % remove the last taper because the last slepian taper is always messy
            tap = tap(1:(end-1), :)';
            ntaper = size(tap,2);
            assert(ntaper>=1,"insufficient number of tapers. increase factor or select 'wavelet'\n");
        case 'hanning'
            tap = hanning(width)';
            tap = tap./norm(tap, 'fro');
        otherwise
            error("select 'wavelet', 'dpss' or 'hanning' as taper method");
    end
    acttapnumsmp = size(tap,1);
    if size(tap,2) == nsample
        prezer = [];
        pstzer = [];
    else
        ins = ceil(nsample./2) - floor(acttapnumsmp./2);
        prezer = zeros(ins,1);
        pstzer = zeros(nsample - ((ins-1) + acttapnumsmp)-1,1);
    end
    ind  = (-(acttapnumsmp-1)/2 : (acttapnumsmp-1)/2)'   .*  ((2.*pi./Fs) .* freq);
    if strcmp(taper, 'dpss')
        wavout = complex(zeros(nsample,ntaper));
        for tapi = 1:ntaper
            wavout(:,tapi) = complex(vertcat(prezer,tap(:,tapi).*cos(ind),pstzer), vertcat(prezer,tap(:,tapi).*sin(ind),pstzer));
        end
    else
        wavout = complex(vertcat(prezer,tap.*cos(ind),pstzer), vertcat(prezer,tap.*sin(ind),pstzer));
    end
    end
end

function [cfg, ft_trldat] = jd_freqanalysis(cfg, Data, tsp)
% this function prepares the cfg for ft_freqanalysis and then runs
% ft_freqanalysis various times depending on input parameters

% prepare cfg
% the mtmconvol and wavelet options return as output the power values
% at the exact time points specified by toi. This implies that there is no
% temporal averaging over and above that done by the
% wavelet. in other words: each returned power value represents a weighted
% sum of signal power around the toi timepoints. So if the tois are
% further apart than the wavelet width then you will skip time
% points, and if the tois are closer then you will generate some
% temporal smoothing (adjacent bins will contain similar information).
% However, this type of smoothing is irrelevant because the tf plot will
% simply consist of smaller pixels. So technically you could set toi to
% 'all', but this would take forever to compute, so we let them overlap
% less.
% set method-speific parameters
if strcmp(cfg.averagebltrials,'no')
    cfg.keeptrials = 'yes';
end
clear cfg.lowfreq cfg.highfreq cfg.numfreq cfg.freqscale cfg.kernelN

% run ft_freqanalysis for stimulus and baseline trials separately
blt = Data.sessionData.baselinetrials;
cfg.trials = ~blt;
ft_trldat = ft_freqanalysis(cfg, Data);

timeToSamp = Data.time{1} >= tsp(1) & Data.time{1} <= tsp(2);
Data.trial(blt) = cellfun(@(x) x(:,timeToSamp), Data.trial(blt), 'UniformOutput', false);
Data.time(blt) = cellfun(@(x) x(:,timeToSamp), Data.time(blt), 'UniformOutput', false);
cfg.trials = blt;
cfg.keeptrials = 'yes';
cfg.toi = 'all'; % calculate power for all timepoints (within tsp)
ft_bldat = ft_freqanalysis(cfg, Data);

% perform baseline correction
if strcmp(cfg.output, 'pow')
    baseline = mean(ft_bldat.powspctrm,4,'omitnan');
    switch cfg.averagebltrials
        case 'no' % use closest baseline for correction
            % perform conrado's calculation
%             baseline = baseline./sum(baseline,3); % transform frequencies to proportion of total power
            ft_trldat.powspctrm = ft_trldat.powspctrm./sum(ft_trldat.powspctrm,3);
            blMatch = dsearchn(find(Data.sessionData.baselinetrials),(1:length(Data.sessionData.baselinetrials))');
            blMatch = blMatch(~Data.sessionData.baselinetrials); % find closest baseline for each trial
            assert(length(blMatch) == size(ft_trldat.trialinfo,1));
            % run baseline correction function
            for Ti = 1:unique(blMatch)
                ft_trldat.powspctrm(blMatch == Ti,:,:,:) = baseline_correction(ft_trldat.powspctrm(blMatch == Ti,:,:,:), squeeze(baseline(Ti,:,:)), cfg.contrast);
            end
        case 'mean' % use mean of all baselines for correction
            baseline = mean(baseline,1,'omitnan'); % average across trials
            ft_trldat.powspctrm = baseline_correction(ft_trldat.powspctrm, squeeze(baseline), cfg.contrast); % 'relchange' is percent change
        case 'median' % use median of all baselines for correction (good for low N or not normaly distributed)
            baseline = median(baseline,1,'omitnan'); % average across trials
            ft_trldat.powspctrm = baseline_correction(ft_trldat.powspctrm, squeeze(baseline), cfg.contrast); % 'relchange' is percent change
        otherwise
            error('incompatible value for cfg.averagebltrials');
    end
elseif strcmp(cfg.output, 'fourier') && strcmp(cfg.averagebltrials, 'no')
    baseline = mean(ft_bldat.fourierspctrm,4,'omitnan');   
%     baseline = baseline./sum(baseline,3); % transform frequencies to proportion of total power
    ft_trldat.fourierspctrm = ft_trldat.fourierspctrm./sum(ft_trldat.fourierspctrm,3);
    blMatch = dsearchn(find(Data.sessionData.baselinetrials),[1:length(Data.sessionData.baselinetrials)]');
    blMatch = blMatch(~Data.sessionData.baselinetrials); % find closest baseline for each trial
    assert(length(blMatch) == size(ft_trldat.trialinfo,1));
    % run baseline correction function
    for Ti = 1:unique(blMatch)
        ft_trldat.fourierspctrm(blMatch == Ti,:,:,:) = baseline_correction(ft_trldat.fourierspctrm(blMatch == Ti,:,:,:), squeeze(baseline(Ti,:,:)), 'normchange');
    end
    cfg.baseline = 'no'; % error prone patch. keep an eye on it
end
end   

function data = baseline_correction(data, baseline, baselinetype)
    % arrange arrays
    dperm = size(data); % store original dimord
    bperm = size(baseline);
%     permf = dperm;
%     while all(permf(1:length(bperm)) ~= bperm)
%         permf = permute(dperm,randperm(length(dperm)));
%     end
    if all(dperm(1:length(bperm)) ~= bperm)
        data = permute(data,[2, 3, 1, 4]); % standard permutation (to: chan, freq, trl, timebin)
        baseline = repmat(baseline,1,1,dperm(1),dperm(4));
    else
        baseline = repmat(baseline,1,1,dperm(3));
    end
%         [~, do, di] = intersect(size(data),iperm,'stable');
    if (strcmp(baselinetype, 'absolute'))
      data = data - baseline;
    elseif (strcmp(baselinetype, 'relative'))
      data = data ./ baseline;
    elseif (strcmp(baselinetype, 'relchange'))
      data = 100*(data - baseline) ./ baseline; % percent change from baseline
    elseif (strcmp(baselinetype, 'normchange')) || (strcmp(baselinetype, 'vssum'))
      data = (data - baseline) ./ (data + baseline);
    elseif (strcmp(baselinetype, 'db'))
      data = 10*log10(data ./ baseline);
    elseif (strcmp(baselinetype,'zscore'))
        stdVals = repmat(nanstd(data(:,:,baselineTimes),1, 3), [1 1 size(data, 3)]);
        data=(data-baseline)./stdVals;
    else
      ft_error('unsupported method for baseline normalization: %s', baselinetype);
    end
    if all(dperm(1:length(bperm)) ~= bperm)
        data = permute(data,dsearchn(size(data)',dperm')); % permute back to original dimord
    end
end

function plotTimeFreq(curatedData, baselinetime, varargin) % plot time-frequency per channel
numfoi = 30; % number of frequencies of interest
lowfoi = 9; % lowest frequency of interest
hifoi = 90; % higest frequency of interest
cycPerFreq = 3; % determines time length basd on foi. (higer number = higer signal to noise but longer time windows)

cfg= [];
cfg.method = 'wavelet';
cfg.output  = 'pow';
if ~isempty(varargin)
    cfg.channel = varargin{1};
    nLoops = length(varargin{1});
else
    nLoops = length(curatedData.label);
end
%                     through multi-tapering. Note that 4 Hz smoothing means
%                     plus-minus 4 Hz, i.e. a 8 Hz smoothing box.
cfg.foi        = linspace(lowfoi, hifoi, numfoi); % vector 1 x numfoi, frequencies of interest (use log10 scale if you are interested in low-end of spectrum)
% cfg.taper      = 'hanning'; % slepian sequence taper 'dpss'
% cfg.t_ftimwin  = cycPerFreq./cfg.foi; % vector 1 x numfoi, length of time window (in seconds)
cfg.toi        = curatedData.time{1}; %vector 1 x numtoi, the times on which the analysis
                   %windows should be centered (in seconds)
% cfg.tapsmofrq = (40000/1600)./cfg.t_ftimwin; % vector 1 x numfoi, the amount of spectral smoothing
cfg.channel = {'CSC17'}
cfg.trials = 165;
% convolve
tf_data = ft_freqanalysis(cfg,curatedData);
% 1/f correction (dB of percent change
% define baseline period
% convert baseline window time to indices
% [~,baselineidx(1)]=find(tf_data.time <= baselinetime(1),1,'last');
% [~,baselineidx(2)]=find(tf_data.time <= baselinetime(2),1,'last');
% baseline_power = mean(tf_data.powspctrm(:,:,baselineidx(1):baselineidx(2)),3);
% dbconverted = 10*( bsxfun(@minus,log10(tf_data.powspctrm),log10(baseline_power)));

% plot result
cfg = [];
cfg.masknans = 'yes';
cfg.baseline = 'yes'; %baselinetime;
cfg.baselinetype = 'db'; % 'relchange' is percent change
cfg.colorbar = 'yes';
figure('Name','time-frequency per channel');

for chani = 1:nLoops
    cfg.title = tf_data.label(chani);
    nexttile
%     hold on
%     contourf(squeeze(tf_data.powspctrm(chani,:,:)),'linecolor','none');
    ft_singleplotTFR(cfg,tf_data);    
%     set(gca,'ytick',cfg.foi,'yscale','lin','xlim',[-300 1300]);
%     title('Color limit of -12 to +12 dB');
%     hold off
end
end

function compute_snr(cfg, tf_curatedData)
nsamp = size(ft_trldat.trialinfo,1);
switch cfg.output
    case 'pow'
        snr_tf = zeros(length(cfg.foi),nsamp);

    case 'fourier'

end
end

