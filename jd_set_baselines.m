function tsp = jd_set_baselines(data)
% setup figure
h = figure('units','normalized','outerposition',[0 0 1 1]);%,'menu','none');
bottoms = [0.55, 0.55, 0.05, 0.05];
lefts = [0.05, 0.5, 0.05, 0.5];
for idx = 1:4
    info.ax(idx) = axes(h,'Position',[lefts(idx),bottoms(idx),0.4,0.4]);
    info.ax(idx).NextPlot = 'add';
    info.ax(idx).SortMethod = 'childorder';
    grid on
end

% the info structure will be attached to the figure
% and passed around between the callback functions
info.start = true;
bl = logical(data.sessionData.baselinetrials);
info.bl.data = data.trial(bl);
info.bl.time = data.time(bl);
info.trl.data = data.trial(~bl);
info.trl.time = data.time(~bl);
info.chan = data.label;
info.ichan = 1;
info.trls = 1:numel(info.trl.data);
info.itrls = info.trls;
info.ravg = 1;
info.nyquist = data.fsample/2;
info.quit = 0;
try
    info.timwin = [{[0,data.sessionData.stimulusParameters.baselineDuration]},{[-data.sessionData.stimulusParameters.ITI,0]}];
catch
    info.timwin = [{[0,5]},{[0,3]}];
end
info.freqwin = [0,100];
info.axflag = logical(true(size(info.ax)));
info.avgflag = logical(false(size(info.ax)));

% setup controls
average = sprintf('mean \x00B1 95%% CI');
info.ui.blAvgTime   = uicontrol(h,'units','normalized','position',[0.05 0.95 0.07 0.03],'String',average, ...
                    'Callback',@avg1,'Style','checkbox');
info.ui.trlAvgTime  = uicontrol(h,'units','normalized','position',[0.5 0.95 0.07 0.03],'String',average, ...
                    'Callback',@avg2,'Style','checkbox');
info.ui.blAvgFreq   = uicontrol(h,'units','normalized','position',[0.05 0.45 0.07 0.03],'String',average, ...
                    'Callback',@avg3,'Style','checkbox');
info.ui.trlAvgFreq  = uicontrol(h,'units','normalized','position',[0.5 0.45 0.07 0.03],'String',average, ...
                    'Callback',@avg4,'Style','checkbox');
info.ui.msgRavg     = uicontrol(h,'units','normalized','position',[0.91 0.75 0.07 0.03],'String','running mean width:', ...
                    'Style','text');
info.ui.setRavg     = uicontrol(h,'units','normalized','position',[0.92 0.7 0.07 0.05],'String',info.ravg, ...
                    'Callback',@setRavg,'Style','edit');
info.ui.msgChan     = uicontrol(h,'units','normalized','position',[0.91 0.65 0.07 0.03],'String','channel:', ...
                    'Style','text');
info.ui.setChan     = uicontrol(h,'units','normalized','position',[0.92 0.6 0.07 0.05],'String',info.chan, ...
                    'Callback',@setChan,'Style','popupmenu');
info.ui.msgTrl     = uicontrol(h,'units','normalized','position',[0.91 0.552 0.07 0.03],'String','trials:', ...
                    'Style','text');
info.ui.setTrl      = uicontrol(h,'units','normalized','position',[0.92 0.201 0.07 0.35],'String',info.trls, ...
                    'Callback',@setTrl,'Style','listbox','Min',2,'Max',length(info.trls));
info.ui.select      = uicontrol(h,'units','normalized','position',[0.92 0.15 0.07 0.05 ],'String','select','Callback',@select);
info.ui.quit        = uicontrol(h,'units','normalized','position',[0.92 0.05 0.07 0.05 ],'String','close','Callback',@stop);

% create time window lines
info.roi.BLBeg = drawline(info.ax(1),'Color','g','Deletable', false, ...
                'InteractionsAllowed', 'translate', 'Position', ...
                [info.timwin{1}(1), info.timwin{1}(1); -500, 500]');
info.roi.BLEnd = drawline(info.ax(1),'Color','r','Deletable', false, ...
                'InteractionsAllowed', 'translate', 'Position', ...
                [info.timwin{1}(2), info.timwin{1}(2); -500, 500]');
info.roi.STBeg = drawline(info.ax(2),'Color','g','Deletable', false, ...
                'InteractionsAllowed', 'translate', 'Position', ...
                [info.timwin{2}(1), info.timwin{2}(1); -500, 500]');
info.roi.STEnd = drawline(info.ax(2),'Color','r','Deletable', false, ...
                'InteractionsAllowed', 'translate', 'Position', ...
                [info.timwin{2}(2), info.timwin{2}(2); -500, 500]');
addlistener(info.roi.BLBeg,'ROIMoved',@newX1);
addlistener(info.roi.BLEnd,'ROIMoved',@newX2);
addlistener(info.roi.STBeg,'ROIMoved',@newX3);
addlistener(info.roi.STEnd,'ROIMoved',@newX4);

guidata(h,info);

% interactivity loop
interactive = 1;
while interactive && ishandle(h)
    redraw(h);
    info = guidata(h);
    if info.quit == 0
        uiwait; %(h)
    else
        tsp = [info.timwin{1}, info.timwin{2}];
        delete(h);
    break
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function redraw(h,~)
if ~ishandle(h)
  return
end
info = guidata(h);
redrawax = find(info.axflag);
for iax = redrawax
    % set data to plot
    if mod(iax,2)
        plotdat = cellfun(@(x) x(info.ichan,:), info.bl.data, 'UniformOutput', false);
        time = info.bl.time;
        z = 1:numel(plotdat);
        trl = 'baseline';
    else
        plotdat = cellfun(@(x) x(info.ichan,:), info.trl.data, 'UniformOutput', false);
        time = info.trl.time;
        z = info.itrls;
        trl = 'trial';
    end
    if iax >= 3 % frequency domain
        ix = ~mod(iax,2)+1;
        Xax = cell(1,numel(plotdat));
        for trli = 1:numel(plotdat)
            timetosamp = time{trli} >= info.timwin{ix}(1) & time{trli} <= info.timwin{ix}(2);
            plotdat{trli} = fft(plotdat{trli}(timetosamp)) / sum(timetosamp);
            N = round(numel(plotdat{trli})/2+1); % positive frequencies + DC component
            plotdat{trli} = log(abs(plotdat{trli}(1:N)).^2);
            Xax{trli} = linspace(0,info.nyquist,N)';
        end
        info.ax(iax).XLabel.String = 'freq (Hz)';
        info.ax(iax).YLabel.String = 'power(dB)';
        xlim = info.freqwin;
        typ = 'spectrum';
    else % time domain
        info.ax(iax).XLabel.String = 'time (sec)';
        info.ax(iax).YLabel.String = 'potential \muV';
        xlim = [time{1}(1), time{1}(end)];
        Xax = cellfun(@(x) x',time,'UniformOutput', false);
        typ = 'timeseries';
    end
    plotdat = cellfun(@(x) movmean(x,info.ravg),plotdat,'UniformOutput', false);
    % plot
    if info.start 
        info.avg(iax) = hggroup('Parent',info.ax(iax),'Visible', info.avgflag(iax));
        info.all(iax) = hggroup('Parent',info.ax(iax),'Visible', ~info.avgflag(iax));
    else
        delete(info.avg(iax).Children);
        delete(info.all(iax).Children);
        info.avg(iax).Visible = info.avgflag(iax);
        info.all(iax).Visible = ~info.avgflag(iax);
    end
%     sem = cellfun(@(x) trl_sem(x),plotdat,'UniformOutput', false);
%     fill(info.avg(iax),[Xax; flipud(Xax)],sem,'k','FaceAlpha',0.2);
%     hold on
%     plot(info.avg(iax),Xax,mean(plotdat,2),'b');
%     hold off
    hold on
    for trli = 1:numel(plotdat)
        plot3(info.all(iax),Xax{trli},plotdat{trli},repmat(z(trli),length(plotdat{trli}),1));
    end
    hold off
    info.ax(iax).XLim = xlim;
    info.ax(iax).ZLim = [0, z(end)];
    info.ax(iax).View = [0,90];
    info.ax(iax).Title.String = sprintf('%s %s',trl, typ);
end
if info.start
    info.start = false;
    linkaxes([info.ax(3),info.ax(4)],'y');
end
info.axflag(:) = false;
guidata(h,info);
drawnow
end

function sem = trl_sem(plotdat)
mn = mean(plotdat,2);
sd = std(plotdat,0,2);
sem = sd/sqrt(size(plotdat,2));
sem = [mn+2*sem; flipud(mn-2*sem)];
end

function setChan(h,~)
info = guidata(h);
info.ichan = info.ui.setChan.Value;
info.axflag(:) = true;
guidata(h,info);
uiresume;
end

function setTrl(h,~)
uiwait;
info = guidata(h);
info.itrls = info.trls(info.ui.setTrl.Value);
info.ui.select = 0;
info.axflag(:) = true;
guidata(h,info)
uiresume;
end

function select(h,~)
uiresume;
end

function avg1(h,~)
info = guidata(h);
info.avgflag(1) = info.ui.blAvgTime.Value;
info.axflag(1) = true;
guidata(h,info);
uiresume;
end
function avg2(h,~)
info = guidata(h);
info.avgflag(2) = info.ui.trlAvgTime.Value;
info.axflag(2) = true;
guidata(h,info);
uiresume;
end
function avg3(h,~)
info = guidata(h);
info.avgflag(3) = info.ui.blAvgFreq.Value;
info.axflag(3) = true;
guidata(h,info);
uiresume;
end
function avg4(h,~)
info = guidata(h);
info.avgflag(4) = info.ui.trlAvgFreq.Value;
info.axflag(4) = true;
guidata(h,info);
uiresume;
end

function newX1(h,event)
info = guidata(h);
info.timwin{1}(1) = event.CurrentPosition(1);
info.axflag(3) = true;
guidata(h,info);
uiresume;
end
function newX2(h,event)
info = guidata(h);
info.timwin{1}(2) = event.CurrentPosition(1);
info.axflag(3) = true;
guidata(h,info);
uiresume;
end
function newX3(h,event)
info = guidata(h);
info.timwin{2}(1) = event.CurrentPosition(1);
info.axflag(4) = true;
guidata(h,info);
uiresume;
end
function newX4(h,event)
info = guidata(h);
info.timwin{2}(2) = event.CurrentPosition(1);
info.axflag(4) = true;
guidata(h,info);
uiresume;
end

function setRavg(h,~)
info = guidata(h);
info.ravg = str2double(info.ui.setRavg.String);
info.axflag(:) = true;
guidata(h,info);
uiresume;
end

function stop(h,~)
info = guidata(h);
info.quit = 1;
guidata(h,info)
uiresume;
end

