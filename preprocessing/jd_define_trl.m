function [bi, output] = jd_define_trl(path, hdr, eventNr, baselineNr, sessionData)
% this function is an edited combination of Lianne's functions:
% - fl_load_analog_events
% - getNlxEvents
% - fl_trialdef
% this function takes as input a folder that contains an Events.nev file
% and a structCC.mat file. The former contains the timestamps and event
% numbers while the latters contains information on the experimental condition presented
% during each trial. From this folder, the function reads the
% abovementioned files, transforms the values field of Events.nev and
% combines both files into a single object.


%% set trial time window (stimulus duration surrounded by 2 full ITIs)
secBefore = 0.5; % sessionData.stimulusParameters.ITI;
secAfter = 3.5; % sessionData.stimulusParameters.stimulusDuration + secBefore;
blsecBefore = 0.5; % sessionData.stimulusParameters.ITI;
blsecAfter = 3.5; % sessionData.stimulusParameters.stimulusDuration + secBefore;

%% load events (fl_load_analog_events)
% fixed parameters
% bitword = 2;
% transform = false;


 if isunix
    events = ft_read_event(fullfile(path,'Events.nev')); % Load in Neuralynx events via Fieldtrip
%     events = ft_read_event('Events.nev');
 else
     events = ft_read_event(path);
 end

%% transform values field in Events.nev (getNlxEvents)

%sample    = [ev.sample]'; %sample is not available
% timestamp = double([events.timestamp]');
val       = [events.value]';       
if max(val(:)) > 256 % only occurs with old data from analog NLX system
    valbin = de2bi(val); % dec2bin doesn't work
    value = bi2de(valbin(:,1:8));
%     if bitword==1
%         valbin    = valbin(:,1:8); %just the first 8 bit word
%     elseif bitword==2
%         valbin    = valbin(:,9:16);
%     end
% %transform 1es into 0s and 0s into 1es
%     if transform
%         [row,col] = size(valbin);
%         valbin2  = num2str(zeros(row,col),'%d%d%d%d%d%d%d%d');
% 
%         for i=1:row
%             for j=1:col
%                 if strcmp(valbin(i,j),'0')
%                     valbin2(i,j) = '1';
%                 end
%             end
%         end
%     else
%         valbin2 = valbin;
%     end
%     value = bin2dec(valbin2);
else
    value = val;
end

%% define samples corresponding to trials (fl_trialdef)
% list stimestamps corresponding to trial onset (stimpres)
FTS     = double(hdr.FirstTimeStamp); 
if max(val(:)) > 256 % only occurs with old data from analog NLX system
    selEventsTs = double([events(value == eventNr).timestamp]); % all stimPres
    trl         = [selEventsTs'-secBefore*10^6, selEventsTs'+secAfter*10^6, -(ones(length(selEventsTs),1)* secBefore*10^6)]; % trial timestamps
    % use pretrial and posttrial as baselines
    firstTrialidx = [events.timestamp] == selEventsTs(1);
    bldur = events(firstTrialidx).timestamp - FTS; 
    pre = double([selEventsTs(1), selEventsTs(1)+bldur, 0]);
    lastTrialidx = [events.timestamp] == selEventsTs(end);
    post = double([events(lastTrialidx).timestamp, hdr.LastTimeStamp,0]);
    bi = [1; zeros(size(trl,1),1); 1]; % beginning and end are baselines
    trl = [pre; trl; post];
else
    selEventsTs = double([events(value == eventNr | value == baselineNr).timestamp]); % all stimPres
    % create logical array for finding baseline events
    bi = [value == eventNr | value == baselineNr, value == baselineNr];
    bi = bi(bi(:,1),2);
    % set special trial duration for baseline window (only for Lianne's data)
    % apply predefined time-window to each timestamp, and transform to sample
    trl = nan(numel(bi),3);
    trl(bi,:)          = [selEventsTs(bi)'-blsecBefore*10^6, selEventsTs(bi)'+blsecAfter*10^6, double(-ones(length(selEventsTs(bi)),1)* blsecBefore*10^6)]; % baseline timestamps
    trl(~bi,:)         = [selEventsTs(~bi)'-secBefore*10^6, selEventsTs(~bi)'+secAfter*10^6, double(-ones(length(selEventsTs(~bi)),1)* secBefore*10^6)]; % trial timestamps
end
trlsamp     = round([(trl(:,1)-FTS)./hdr.TimeStampPerSample (trl(:,2)-FTS)./hdr.TimeStampPerSample trl(:,3)./hdr.TimeStampPerSample]);
%% label each trial according to its condition
trlInfo(1:size(trlsamp,1),1:3) = 999;
if max(val(:)) > 256 % only occurs with old data from analog NLX system
    assert(all(sessionData.ModulationRate == sessionData.vecPresModulationRate));
    assert(all(sessionData.VisualSpeed == sessionData.vecPresVisualSpeed));
    % append trial data visual frequency 
    trlInfo(~bi,2) = sessionData.vecPresVisualSpeed';
    % append trial data audio frequency    
    sessionData.vecPresModulationRate = sessionData.vecPresModulationRate';
    sessionData.vecPresModulationRate(sessionData.vecPresModulationRate == 999) = 0;% replaces 999 value with 0 in visual-only condition
    trlInfo(~bi,3) = sessionData.vecPresModulationRate;
else
% % %     if size(sessionData.trialData.presOrder,2) ~= sessionData.stimulusParameters.repetitions
% % %         sessionData.trialData.presOrder = sessionData.trialData.presOrder';
% % %     end
    sessionData.trialData.presOrder = sessionData.trialData.presOrder';
    assert(size(sessionData.trialData.presOrder,1) == size(sessionData.trialData.presList,1));
    % append trial data visual frequency 
    visFreq = sessionData.trialData.presList(sessionData.trialData.presOrder(:),2);
    trlInfo(~bi,2) = visFreq;
    % append trial data audio frequency     
    sessionData.trialData.audioTextures{5,2} = 0; % replaces string variable with 0 in visual-only condition
    audConds = cell2mat(sessionData.trialData.audioTextures(:,2));
    trlInfo(~bi,3) = audConds(sessionData.trialData.presList(sessionData.trialData.presOrder(:),3));
end
% compute conditions
trlInfo(~bi,1) = 0; % incongruent conditions
trlInfo(trlInfo(:,2) == trlInfo(:,3) & ~bi,1) = 1; % congruent conditions
trlInfo(trlInfo(:,3) == 0,1) = 2; % visual only conditions
if ~all(all(trlInfo(~bi,:) ~= 999)) && ~all(all(trlInfo(bi,:) == 999))
    error('something went wrong with assignment of conditions');
end

%     audFreq = zeros(length(visFreq),1);
%     ctr = 1;
%     for i = numel(sessionData.trialData.audioTextures)/2+1 : numel(sessionData.trialData.audioTextures)
%         audFreq(ctr) = [audFreq; sessionData.trialData.audioTextures{i}];
%         ctr = ctr+1;
%     end
%     audFreq = audFreq(sessionData.trialData.presList(sessionData.trialData.presOrder(:),3));
%     cond = zeros(numel(visFreq),1);
%     cond(visFreq == audFreq) = 1; % congruent conditions
%     cond(audFreq == 0) = 2; % visual only conditions
%     output = [trlsamp, cond, visFreq, audFreq] ;
output = [trlsamp, trlInfo];

%% trl output structure

% % trl(:).trial = events.number;
% trl = struct('trl', trlsamp);
% % trl = struct('visFreq', horzcat(structCC.VisualSpeed));
% % trl = struct('audFreq', horzcat(structCC.ModulationRate));
% % trl = struct('cond', cond);
% 
% % trl.trl = trlsamp;
% trl.visFreq = horzcat(structCC.VisualSpeed)';
% trl.audFreq = horzcat(structCC.ModulationRate)';
% trl.cond = cond';

end