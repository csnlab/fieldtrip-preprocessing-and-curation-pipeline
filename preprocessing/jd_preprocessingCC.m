%% Booleans
doSave      = 1;
doPlot      = 0;
doLocal     = 1; 
saveSpike   = 0;
channelWise = 0; % set to 1 if simultaneous preprocessing takes up too much memory
analysisType = 'Concentric'; %'Multisensory', 'Pupil'

warning('off','all') % turn off pesky 'no NLX toolbox blablabla' warnings
% tic;
%% Concentric Circles
% define paths and files where the data is stored
if doLocal == 0
    root = '/data/jdaatse/concentric_circles';
    datafolder = '/data/home/lklaver1/Data/Passive/ConcentricCircles';
    addpath (genpath(root));
    addpath (genpath(datafolder));
    resultsfolder = fullfile(root,'processed_data');
    addpath (genpath(resultsfolder));
    addpath /home/jdaatse/matlab/fieldtrip-20191028;
    ft_defaults
else
    root = 'C:/Users/J.M. van Daatselaar/Documents/PhD/Bosman Vittini/projects/concentric circles';
    datafolder = fullfile(root, '/raw_data');
    resultsfolder = fullfile(root, '/processed_data');
    addpath (genpath(root));
    addpath (resultsfolder);
end

%% create trl function
% the trl function cuts up the recording into trials with specified windows
% this section creates the trl function and then runs it over the data,
% creating a new dataset in the process

% trl a priori parameters
% secBefore = 0.3;
% secAfter = 1.3;
eventNr = 9; % This refers to stimulus onset in my case
baselineNr = 3000; % this is start baseline
%% loading, preprocessing and storage of data
% because the system is prone to crash, we will preprocess the data one
% channel at a time and append it to a dataframe
% general preprocessing parameters
cfg           = [];
%cfg.datatype  = 'neuralynx_ncs';
% cfg.hdr       = hdr;
cfg.lpfilter  = 'yes';
cfg.lpfreq    = 300;
cfg.demean    = 'yes'; 
cfg.detrend   = 'yes';
cfg.dftfilter = 'yes';
cfg.padding   = 2;
cfg.padtype   = 'data';

% resample
cfg2            = [];
% cfg2.method     = 'resample';
% cfg2.resamplefs =  1010; % 30303/1010=30,0030 (closest to integer)

sessions = dir_crawler(datafolder, '\.nev$');
% clock = zeros(length(sessions));
for sesi = 1:length(sessions)
    session = fileparts(sessions{sesi});
    fprintf('session: %s \n', session);
    cfg.hdr = ft_read_header(session);
    matFile = dir(fullfile(session,'*.mat*'));
    sessionData = load(matFile.name); % loads structure called sessionData
    fieldName = fieldnames(sessionData);
    sessionData = eval(sprintf('sessionData.%s',fieldName{1}));
    if cfg.hdr.Fs == 30303
        cfg2.resamplefs =  1010; % 30303/1010=30,0030 (closest to integer)
    elseif cfg.hdr.Fs == 32000
        cfg2.resamplefs = 1600;
    else
        error('original Fs of %s session %s is 30303 nor 32000', ferrets{feri}, sessions{sesi})
    end
    [sessionData.baselinetrials, cfg.trl] = jd_define_trl(session, cfg.hdr, eventNr, baselineNr, sessionData);
%     cfg.trl = cfg.trl(1:5:end,:);
    if channelWise == 1
        nchans = cfg.hdr.nChans;
         fprintf('nChans: %s \n', nchans);
        cfg.dataset = session;
        tmp = cell(nchans,1);
        for chani = 1:nchans % preproces each channel of this session separately
            % add the data to the cfg file and perform preprocessing
            cfg.channel = chani;
            fprintf('processing channel %s \n', chani);
            datp          = ft_preprocessing(cfg);
            tmp{chani}          = ft_resampledata(cfg2,datp);
%             tmp          = ft_preproc_lowpassfilter(tmp, cfg2.resamplefs, cfg.lpfreq);
            % create a structure to hold all channels
            clear datp
        end
        cfga = []; % empty configfile for appending
        processedData = ft_appenddata(cfga, tmp{:}); % expand all cels into input variables
        clear tmp
    else
        cfg.dataset = session;
        processedData          = ft_preprocessing(cfg);
        processedData          = ft_resampledata(cfg2,processedData);
    end
    processedData.time = jd_DSP_delay_compensation(processedData);
    [ferret, sessionName, ~] = fileparts(session);
    [~, ferret, ~] = fileparts(ferret);
    processedData.ferret = ferret;
    processedData.sessionData = sessionData;
    % save the rawData of this session in the results folder
    if doSave == 1
        fileName = sprintf('%s_%s',ferret, sessionName);
        save(fullfile(resultsfolder,fileName), 'processedData', '-v7.3');
        clear processedData
    end
    clear rawData
    clear ferret
    clear sessionName
    cfg = rmfield(cfg, {'trl', 'hdr', 'dataset'});
%     clock(sesi) = toc;
end

