function time = jd_DSP_delay_compensation(time,hdr, session)
% this function checks whether the Digital Signal Processing filter delay
% was compensated for by cheetah, and corrects this if it is not the case.
% Note that this function was designed for the Neuralynx Digital Lynx SX, 

% sanity checks
assert(strcmp('DigitalLynxSX',hdr.orig(1).hdr.HardwareSubSystemType),'DSP delay compensation only works for NLX DigitalLynxSX');
assert(isfield(hdr.orig(1).hdr,'DspDelayCompensation'),'missing DSP information'); %not the caase in NLX analog systems

%% perform delay compensation
if strcmp(hdr.orig(1).hdr.DspDelayCompensation,'Disabled')
    delays = nan(size(hdr.orig));
    for ci = 1:size(delays,1)
        delays(ci) = hdr.orig(ci).hdr.DspFilterDelay_ms; % delay is microsecs, not millisecs!
    end
    % make sure all channels have the same delay
    delay = unique(delays)*1e-6; % transform to microseconds
    assert(numel(delay) == 1,sprintf('varying numbers of DSP delays in session %s\n',session))
    % compensate for this delay in processed data
    time = cellfun(@(x) x-delay, time, 'UniformOutput', false);
else
    return
end
end